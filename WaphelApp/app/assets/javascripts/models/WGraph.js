define([
	'jquery',
	'underscore',
	'backbone',
	'models/WNode',
	'models/WLink'
], function($,_,Backbone,WNode,WLink){
	

	var WGraph = Backbone.Model.extend({

		model : {},
		nodes : [],
		links : [],
		relevantNodeCount : 7,
		startNode : {},
		currentNode : {},

		initialize: function() {
			this.set({"currentNode" : this.get("startNode")});
			this.nodes.push(this.get("startNode"));
		},

		setCurrentNode: function(nextID) {
			var currentID = this.get("currentNode").id;

			//console.log("nodes 0", this.nodes);
			//console.log("current ID", currentID, " nextID", nextID);
			if (nextID === currentID) {
				console.log("Node already current");
			} else {
				var nextNode  = _.where(this.nodes,{"id" : nextID})[0];
				console.log("next node", nextNode);
				if (nextNode) {
					this.addNodeToSearchPath(nextNode,this);
					console.log("radius", nextNode.radius);
					nextNode.radius = 150;

				}
			}
			//console.log("nodes 1", this.nodes);
		},

		addNodeToSearchPath: function(nextNode, that) {
			if (!nextNode.belongsToSearchPath) {
				nextNode.belongsToSearchPath = true;
				//nextNode.lastOnSearchPath = true;
				this.get("currentNode").lastOnSearchPath = false;
				console.log("nextNode", nextNode);
				if (nextNode.nodeList.length < 1) {
					that.setupNodeData(nextNode);
				}
				if (nextNode.htmlContent === null) {
					that.getArticleContent(nextNode,that);
				}
				/*
				_.each(_.where(that.links,{"target" : nextNode}),function (li) {
					li.belongsToSearchPath = true;
					//console.log("new link", li);
				});
				*/						
			}

			var newLinks = [];
			for (var it = 0; it < that.links.length; it++) {
				var li = that.links[it];
				if (li.target.belongsToSearchPath) {
					newLinks.push(li);
				}
			}
			that.links = newLinks;

			var newNodes = [];
			for (var it = 0; it < that.nodes.length; it++) {
				var no = that.nodes[it];
				if (no.belongsToSearchPath) {
					newNodes.push(no);
				}
			}
			that.nodes = newNodes;

			for (var it = 1; it <= that.relevantNodeCount; it++) {
				var node = nextNode.nodeList[it].node;
				if ($.inArray(node,that.nodes) === -1) {
					//node.x = nextNode.x + Math.random()*2;
					//node.y = nextNode.y + Math.random()*2;
					that.nodes.push(node);
					var link = new WLink();
					link.create(nextNode,node);
					that.links.push(link);
				}
			}

			//that.links = _.where(that.links,{"belongsToSearchPath" : true});
			//that.nodes = _.where(that.nodes,{"belongsToSearchPath" : true});
			


			that.set({"currentNode" : nextNode});					
		},

		removeNodesFromSearchPath: function() {

			console.log("currentNode", this.get("currentNode"));

			var newLinks = [];
			
			for(var it = 0; it < this.links.length; it++) {
				if(this.links[it].target.belongsToSearchPath) {
					newLinks.push(this.links[it]);
				}
			}

			this.links = newLinks;

			var nodesToRemove = [];

			for(var it = 1; it <= this.relevantNodeCount; it++) {
				nodesToRemove.push(this.get("currentNode").nodeList[it].node);
			}

			this.nodes = _.difference(this.nodes,nodesToRemove);

			
		},

		
		setupNodeData: function(theNode) {
			var theTitle = theNode.title;
			$.ajax({
        		type : 'GET',
        		url : '/searchResult/' + theTitle,
        		async: false,
        		success : function(data) { 
        			console.log("data ", data);
        			theNode.setNodeData(data);
        		}
    		});
		},

		getArticleContent: function(theNode,that) {

			var theTitle = theNode.title;

			//kis valtozas
			
			//$.support.cors = true;
			$.ajax({
        		type : 'POST',
        		url : 'http://en.wikipedia.org/w/api.php?action=parse&page='+ encodeURI(theTitle) + '&prop=text',
        		data: {
			        format: 'json'
			    },
			    dataType: 'jsonp',
        		async: false,
        		cmoplete : function(data) {
        			console.log("wiki", data);
        			//theNode.articleContent = data;
        			var htmlTitle = "<h1>" + data.parse.title + "</h1>";
        			theNode.setHtmlContent(htmlTitle + data.parse.text["*"]);
        			that.trigger('change');
        			//console.log("HTML content wgraph", theNode.htmlContent);
        		}
    		});
    		//console.log("HTML content wgraph", theNode.htmlContent);
		}

		

	});

	return WGraph;
});