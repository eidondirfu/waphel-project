define([
	'jquery',
	'underscore',
	'backbone'
], function($,_,Backbone){
	
	var WNode = function(){

		var my = {}, that = {};

		my.isInt = function (n) {
   			return typeof n === 'number' && parseFloat(n) == parseInt(n, 10) && !isNaN(n);
		};

		my.id = "fakeId";
		my.outNodeData = []; //a JSON containing wikipedia article titles
		my.outNodes = []; //an array of WNode objects
		my.articleContent = {};
		my.htmlContent = null;
		my.belongsToSearchPath = false;
		my.belongsToCurrentBranch = false;
		my.isStartNode = false;
		my.isCurrentNode = false;
		my.pathID = "";
		my.depth = 0;
		my.parentWNode = undefined;
		my.title = "fakeTitle";
		my.lambda = 0;
		my.hover = false;

		my.jq = function (input) {
			return input.replace(/\W/g, '');
		};

		that.create = function(title,pathID,depth,parent) {
			my.title = title;
			my.pathID = pathID;
			my.id = my.jq(title);
			if (depth) {
				my.depth = depth;
			}
			if (parent) {
				my.parentWNode = parent;
			}
		};

		that.setOutNodes = function(theData) {
			my.outNodeData = theData;
			var r = 1;
			var nodeData = my.outNodeData;
			var nodeCount = nodeData.length;
			var interval = 1/nodeCount;

			for (var it = 0; it < nodeCount; it++) {
				var title = nodeData[it].title;
				if (my.title !== title) {
					var n = new WNode();
					n.create(title,my.pathID, my.depth + 1,that);
					var nInd = (it + 1) / (nodeCount+1);
					that.addOutNode({"node" : n, "title" : title, "relevance" : r, "normalizedIndex": nInd});
					r -= interval;
					//r = Math.random();					
				}
			}
		};

		that.outNodes = function() {
			return my.outNodes;
		};

		that.addOutNode = function(node) {
			my.outNodes.push(node);
		};

		/////////// GETTERS & SETTERS

		that.id = function(theID) {
			if (theID === undefined) {
				return my.id;
			} else {
				my.id = my.jq(theID);
			}
		};

		that.outNodeData = function(theData) {
			if (theData === undefined) {
				return my.outNodeData;
			} else {
				my.outNodeData = theData;				
			}
		};

		that.articleContent = function(theContent) {
			if (theContent === undefined) {
				return my.articleContent;	
			} else {
				my.articleContent = theContent;
			}			
		};

		that.htmlContent = function(theContent) {
			if (theContent === undefined) {
				return my.htmlContent;	
			} else {
				my.htmlContent = theContent;
			}			
		};

		that.belongsToSearchPath = function(bol) {
			if (bol === undefined) {
				return my.belongsToSearchPath;	
			} else {
				my.belongsToSearchPath = bol;
			}
		};

		that.belongsToCurrentBranch = function(bol) {
			if (bol === undefined) {
				return my.belongsToCurrentBranch;	
			} else {
				my.belongsToCurrentBranch = bol;
			}
		};

		that.isStartNode = function(bol) {
			if (bol === undefined) {
				return my.isStartNode;	
			} else {
				my.isStartNode = bol;
			}
		};

		that.isCurrentNode = function(bol) {
			if (bol === undefined) {
				return my.isCurrentNode;	
			} else {
				my.isCurrentNode = bol;
			}
		};

		that.pathID = function(theID) {
			if (theID === undefined) {
				return my.pathID;	
			} else {
				my.pathID = theID;
			}
		};

		that.depth = function(theDepth) {
			if (theDepth !== undefined && my.isInt(theDepth)) {
				my.depth = theDepth;
			} else {
				return my.depth;
			}
		};

		that.parentWNode = function(parent) {
			if (parent === undefined) {
				return my.parentWNode;
			} else {
				my.parentWNode = parent;
			}
		};

		that.title = function(theTitle) {
			if (theTitle === undefined) {
				return my.title;
			} else {
				my.title = theTitle;				
			}
		};

		that.lambda = function(theLambda) {
			if (theLambda === undefined) {
				return my.lambda;
			} else {
				my.lambda = theLambda;
			}
		};

		that.hover = function(bol) {
			if (bol !== undefined) {
				my.hover = bol; 
			} else {
				return my.hover;	
			}
		};

		return that;
	};

	return WNode;
});