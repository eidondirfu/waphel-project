define([
	'jquery',
	'underscore',
	'backbone',
	'models/WNode',
	'models/WLink'
], function($,_,Backbone,WNode,WLink){
	

	var WPath = function() {
		var my = {}, that = {};

		my.id = "";
		my.isVisible = false;
		my.nodes = [];
		my.links = [];
		my.relevantNodeCount = 7;
		my.startNode = {};
		my.currentNode = {};

		that.createSingle = function(theID,theData) {

			my.id = theID;
			my.startNode = new WNode();
			my.startNode.create(theData[0].title,theID,0);
			my.startNode.isStartNode(true);
			my.startNode.isCurrentNode(true);
			my.startNode.setOutNodes(theData);
			my.startNode.belongsToSearchPath(true);
			my.currentNode = my.startNode;
			my.nodes.push(my.startNode);
			my.addRelevantNodes(my.startNode);
		};

		that.createFromWData = function(theID,theNodes, theLinks, theStartNode, theCurrentNode) {

			my.id = theID;
			my.startNode = theStartNode;
			my.startNode.isStartNode(true);
			my.currentNode = theCurrentNode;
			my.currentNode.isCurrentNode(true);
			my.nodes = theNodes;
			my.links = theLinks;
			//my.addRelevantNodes(my.currentNode);

		};

		that.setCurrentNode = function(theTitle) {
			
			var theNode = _.find(my.nodes,function(n) {
				if (n.title() === theTitle) {
					return true;
				} else {
					return false;
				}
			});
			if (theNode !== undefined) {
				
				theNode.belongsToSearchPath(true);
				my.currentNode = theNode;
				my.cleanSearchPath();
				my.cleanCurrentBranch();
				my.setCurrentBranch(my.currentNode);
			} else {
				console.error("No node in Path with title:", theTitle);
			}				
			
		};

		my.setCurrentBranch = function(theNode){
			theNode.belongsToCurrentBranch(true);
			if (theNode.depth() > 0) {
				//console.log(theNode, "belongs to current branch.");
				my.setCurrentBranch(theNode.parentWNode());
			}		
		};

		my.cleanCurrentBranch = function() {
			$.each(my.nodes, function(i,n) {
				n.belongsToCurrentBranch(false);
			});
		};

		my.cleanSearchPath = function() {
	
			my.links = _.filter(my.links,function(l) {
				return l.target.belongsToSearchPath();
			});
			
			my.nodes = _.filter(my.nodes,function(n) {
				return n.belongsToSearchPath();
			});
			
		};

		my.addRelevantNodes = function(theNode) {
			var outNodes = theNode.outNodes();
			var len = outNodes.length;
			if (len > 0) {
				var it = 0;
				var count = my.relevantNodeCount;
				while (count > 0 && it < len) {
					var node = outNodes[it].node;
					if (!_.contains(my.nodes,node)) {
						node.belongsToSearchPath(false);
						node.pathID(my.id);
						my.nodes.push(node);
						var link = new WLink();
						link.create(theNode,node);
						my.links.push(link);
						count--;
					}
					it++;
				}
			} else {
				console.log("node", node, "has no outnodes!");
			}
		};

		that.addNode = function(theNode) {

			my.nodes.push(theNode);
		};

		that.addLink = function(theLink) {

			my.links.push(theLink);
		};


		/////// GETTERS & SETTERS

		that.nodes = function(theNodes) {

			if(theNodes === undefined) {
				return my.nodes;
			} else {
				my.nodes = theNodes;
			}
			
		};

		that.links = function(theLinks) {

			if(theLinks === undefined) {
				return my.links;
			} else {
				my.links = theLinks;
			}
			
		};

		that.relevantNodeCount = function(theCount) {
			if (theCount === undefined) {
				return my.relevantNodeCount;
			} else {
				my.relevantNodeCount = theCount;
			}
		};

		that.startNode = function(theStartNode) {

			if(theStartNode === undefined) {
				return my.startNode;
			} else {
				my.startNode = theStartNode;
			}
			
		};

		that.currentNode = function(theCurrentNode) {

			if(theCurrentNode === undefined) {
				return my.currentNode;
			} else {
				my.currentNode = theCurrentNode;
			}
			
		};

		that.id = function(theID) {

			if(theID === undefined) {
				return my.id;
			} else {
				my.id = theID;
			}
			
		};

		that.isVisible = function(bol) {

			if(bol === undefined) {
				return my.isVisible;
			} else {
				my.isVisible = bol;
			}
			
		};

		return that;

	};

	return WPath;
});