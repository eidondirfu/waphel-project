define([
	'jquery',
	'underscore',
	'backbone'
], function($,_,Backbone){
	
	var WLink = function(){
		var my = {}, that = {};

		that.len = 0;
		that.source = undefined;
		that.target = undefined;
		my.id = "fakeId";

		that.create = function(source,target) {
			that.source = source;
			that.target = target;
			//my.id = source.id() + target.id();
		};

		//////////////////GETTERS & SETTERS

		that.id = function() {
			return my.id;
		};

		that.belongsToSearchPath = function() {
			if (that.source.belongsToSearchPath() && that.target.belongsToSearchPath()) {
				return true;
			}
			return false;
		};

		that.belongsToCurrentBranch = function() {
			if (that.source.belongsToCurrentBranch() && that.target.belongsToCurrentBranch()) {
				return true;
				console.log("link belongs to current branch", source.title(), "-->", target.title());
			}
			return false;
		};

		that.strokeWidth = function() {
			if (that.belongsToSearchPath()) {
				if (that.belongsToCurrentBranch()) {
					return 10;
				} else {
					return 8;
				}
			} else {
				return 8;
			}
		};


		that.hover = function() {
			if (that.source.hover() && that.target.hover()) {
				return true;	
			}
			return false;
		};

		return that;
	};

	return WLink;
});