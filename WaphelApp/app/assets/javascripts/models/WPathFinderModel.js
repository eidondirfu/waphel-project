define([
	'jquery',
	'underscore',
	'backbone',
	'models/WNode',
	'models/WLink'
], function($,_,Backbone,WNode,WLink){
	

	var WPathFinderModel = function() {
		var my = {}, that = {};

		my.model = {};
		my.pathNodesData = []; //JSON array
		my.nodes = [];
		my.links = [];
		my.startNode = {};
		my.endNode = {};
		that.pathFinderModelChanged = false;

		that.create = function() {
			_.bindAll(this);
		};

		that.pathFinder = function(startNodeTitle, endNodeTitle) {


			var startNode = new WNode();
			startNode.create(startNodeTitle);
			var endNode = new WNode();
			endNode.create(endNodeTitle);

			my.startNode = startNode;
			my.endNode = endNode;

			console.log("startNodeTitle and endNodeTitle in ajax", startNodeTitle, endNodeTitle);
			console.log("my.startNodeTitle and my.endNodeTitle", startNode.title(), endNode.title());

			$.ajax({
        		type : 'GET',
        		url : '/pathFinderResult/' + startNodeTitle + '/' + endNodeTitle,
        		async: false,
        		success : function(data) { 
        			console.log("pathFinder data", data);
        			my.setPathNodesAndLinks(data);
        			my.triggerPathFinderModelChanged();
        		}
    		});
		};

		my.triggerPathFinderModelChanged = function() {
			//that.pathFinderModelChanged = !that.pathFinderModelChanged;
			that.trigger('change:pathFinderModelChanged');

		};

		my.setPathNodesAndLinks = function(theData) {

			my.pathNodesData = theData;
			my.nodes = [];
        	my.links = [];
			var nodeData = my.pathNodesData;
			var nodeCount = nodeData.length;
			
			for (var it = 0; it < nodeCount; it++) {
				var title = nodeData[it].title;
				var n = new WNode();
				n.create(title);
				n.belongsToSearchPath(true);
				that.addPathNode(n);
				
				if(it > 0) {

					var link = new WLink();
					link.create(my.nodes[it - 1], n);
					my.links.push(link);
				}
									
			}

			console.log("pathNodes and pathLinks", my.nodes, my.links);
		};

		that.addPathNode = function(node) {
			my.nodes.push(node);
		};

		



		///////////// GETTERS & SETTERS
		that.model = function(theModel) {
			if (theModel === undefined) {
				return my.model;
			} else {
				my.model = theModel;
			}
		};

		that.nodes = function() {
			return my.nodes;
		};

		that.links = function() {
			return my.links;
		};

		that.startNode = function() {
			return my.startNode;
		};

		that.endNode = function() {
			return my.currentNode;
		};

		///////////// BOILERPLATE
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		//_.extend(that, Backbone.Events);
		var WPFModel = Backbone.Model.extend(constructor.apply(this,arguments));
		that = new WPFModel();
		return that;
	};

	return WPathFinderModel;
});