define([
	'jquery',
	'underscore',
	'backbone',
	'models/WGraphModel'
], function($,_,Backbone,WGraphModel){
	

	var WTextFormatter = function() {
		var my = {}, that = {};

		that.formatHtmlContent = function(htmlContent) {

			
			var fragment = document.createDocumentFragment();
			var myDiv = document.createElement('div');

			myDiv.innerHTML = htmlContent;

			$(myDiv).find("a").each(function(i,n) {

							if($(n).has('img').length) {
								$(n).attr('class','w-text-link-img');
								if($(n).attr('title') && $(n).attr('title').indexOf('Enlarge') === 0) {
									$(n).find('img').attr('class', 'w-img-enlarge');
								} else {
									$(n).find('img').attr('class', 'img-polaroid');
								}
							} else {
								$(n).attr('class','w-text-link');

							}

							if($(n).attr('href').indexOf('/wiki') === 0) {

	        					$(n).attr('href','#');
	        				} else if ($(n).attr('href').indexOf('http://') === 0) {

	        					$(n).attr('target','_blank');
	        				}
							

	        				
	        			});
			$(myDiv).find("span").each(function(i,n) {

				var attr = $(n).attr('class');

				if (typeof attr !== 'undefined' && attr !== false) {
					if(attr.indexOf('editsection') === 0) {

						$(n).remove();
					}
				}	
			});

			$(myDiv).find("table").each(function(i,n) {

				var attr = $(n).attr('class');

				if (typeof attr !== 'undefined' && attr !== false) {
					if(attr.indexOf('infobox vcard') === 0) {

						//$(n).hide();
					}
				}	
			});

			fragment.appendChild(myDiv);

			return myDiv;

			

		};

		return that;
	};

	return WTextFormatter;
});