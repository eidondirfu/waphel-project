define([
	'jquery',
	'underscore',
	'backbone',
	'models/WPath',
	'models/WNode',
	'models/WLink',
	'models/WTextFormatter'
], function($,_,Backbone,WPath,WNode,WLink,WTextFormatter){
	

	var WGraphModel = function() {
		var my = {}, that = {};

		my.pathList = [];		

		my.nodes = [];
		my.links = [];

		my.currentNode = {};

		my.idCount = 0;

		my.relevantNodeCount = 7;

		that.graphModelChanged = false;


		that.create = function(theData) {
			
			_.bindAll(this);
			my.addPathSingle(theData);
			
		};

		my.addPathSingle = function(theData) {

			my.setPathVisibility(false);

			var myPath = new WPath();
			myPath.createSingle("path" + my.idCount,theData);
			my.idCount++;
			myPath.isVisible(true);
			my.pathList.push(myPath);
			my.currentNode = myPath.currentNode();
			my.updateNodesAndLinks();
			my.triggerGraphModelChanged();


		};

		my.addPathSegment = function(theData) {

			//my.setPathVisibility(false);

			_.each(my.nodes,function(n) {

					n.isCurrentNode(false);
					n.isStartNode(false);
				});

			var path = new WPath();
			var pathID = "path" + my.idCount;
			my.idCount++;

			var nodeData = theData;
			var nodeCount = nodeData.length;
			var nodes = [];
			var links = [];
			
			for (var it = 0; it < nodeCount; it++) {
				var title = nodeData[it].title;
				var n = my.getNodeOf(title);
				if(n === undefined) {

					n = new WNode();
					if(it === 0) {
						n.create(title,pathID,it);
					} else {
						n.create(title,pathID,it,nodes[it - 1]);
					}
					

				}
				nodes.push(n);

				n.belongsToSearchPath(true);
				
				if(it > 0) {

					if(my.getLinkOf(nodes[it - 1], n) === undefined) {
						var link = new WLink();
						link.create(nodes[it - 1], n);
						links.push(link);
					}
				}
									
			}

			var startNode = nodes[0];
			var currentNode = nodes[nodeCount - 1];

			nodes = _.difference(nodes,my.nodes);

			path.createFromWData(pathID,nodes,links,startNode,currentNode);
			path.isVisible(true);
			//my.currentNode = path.currentNode();
			my.pathList.push(path);

			my.setupOutNodesFor(currentNode,path);
			//that.setCurrentNodeFromSearchPathNodes(currentNode,path);

			//my.updateNodesAndLinks();

			//my.triggerGraphModelChanged();

			console.log("pathNodes and pathLinks", nodes, links);
		};

		my.setPathVisibility = function(bol) {

			if(bol === true) {
				_.each(my.pathList,function(p) {

					p.isVisible(true);
				});
			} else if(bol === false) {
				_.each(my.pathList,function(p) {

					p.isVisible(false);
				});
			}
		};

		my.updateNodesAndLinks = function() {

			my.nodes = [];
			my.links = [];

			var visiblePaths = _.filter(my.pathList, function(p) {

				return p.isVisible();
			});

			_.each(visiblePaths, function(p,i) {

				my.nodes = _.union(my.nodes,p.nodes());
				my.links = _.union(my.links,p.links());
			});

			_.each(my.nodes, function(n) {

				if(n === my.currentNode) {
					n.isCurrentNode(true);
				} else {
					n.isCurrentNode(false);
				}
			});
		};

		that.updateCurrentPath = function(theTitle) {

			if (theTitle === that.currentNode().title()) {
				console.log("node already current");
			} else {
				var thePath;

				var theNode = my.getNodeOf(theTitle);

				if (theNode !== undefined) {

					thePath = my.getPathOf(theNode);
					console.log("thePath", thePath.id());

					if(thePath !== undefined) {

						that.setCurrentNodeFromSearchPathNodes(theNode,thePath);

					} else {
						console.error("no such path");
					}
					
				} else {
					console.error("No node in Path with title:", theTitle);
				}	
			}
		};

		that.setCurrentNodeFromSearchPathNodes = function(theNode,thePath) {			

			thePath.setCurrentNode(theNode.title());

			if (thePath.currentNode().outNodes().length > 0) {
				my.addRelevantNodes(thePath.currentNode(), thePath);
				my.currentNode = thePath.currentNode();
				my.updateNodesAndLinks();
				my.triggerGraphModelChanged();
			} else {
				my.setupOutNodesFor(thePath.currentNode(),thePath);
			}
				
		};

		my.addRelevantNodes = function(theNode,thePath) {
			var outNodes = theNode.outNodes();
			var len = outNodes.length;
			if (len > 0) {
				var it = 0;
				var count = that.relevantNodeCount();
				while (count > 0 && it < len) {
					var node = outNodes[it].node;
					if (!_.contains(that.nodes(),node)) {
						node.belongsToSearchPath(false);
						node.pathID(theNode.pathID());
						thePath.addNode(node);
						var link = new WLink();
						link.create(theNode,node);
						thePath.addLink(link);
						count--;
					}
					it++;
				}
			} else {
				console.log("node has no outnodes!");
			}
		};


		///TODO use up to date version of $.ajax()
		my.setupOutNodesFor = function(theNode,thePath) {
			var theTitle = theNode.title();
			$.ajax({
				type : 'GET',
				url : '/searchResult/' + theTitle,
				async: false,
				success : function(data) { 
					theNode.setOutNodes(data);
					my.addRelevantNodes(theNode,thePath);
					my.currentNode = theNode;
					my.updateNodesAndLinks();
					my.triggerGraphModelChanged();
				}
			});
		};

		

		my.triggerGraphModelChanged = function() {
			//that.graphModelChanged = !that.graphModelChanged;
			that.trigger('change:graphModelChanged');

		};

		that.setCurrentNodeFromOutNodes = function(theTitle) {

			if (theTitle === that.currentNode().title()) {
				console.log("node already current from text");
			} else {
				var theNodeObject = _.find(that.currentNode().outNodes(),function(n) {
					if (n.title === theTitle) {
						return true;
					} else {
						return false;
					}
				});

				if(theNodeObject !== undefined) {
					var theNode = theNodeObject.node;
					var thePath = my.getPathOf(theNode);					
					thePath.addNode(theNode);
					var link = new WLink();
					link.create(that.currentNode(),theNode);
					thePath.addLink(link);
					theNode.belongsToSearchPath(false);

					console.log("parent node of theNode from text", theNode.parentWNode().title());

					that.setCurrentNodeFromSearchPathNodes(theNode,thePath);
					
				} else {
					console.error("FATAL ERROR ... No such linkNode in currentNode's outNodes with title:", theTitle);

				}				
			}
		};

		my.getPathOf = function(theNode) {

			var thePathID = theNode.pathID();

			var path = _.find(my.pathList,function(n) {
						if (n.id() === thePathID) {
							return true;
						} else {
							return false;
						}
					});

			return path;

		};

		my.getNodeOf = function(theTitle) {

			var node = _.find(my.nodes,function(n) {
						if (n.title() === theTitle) {
							return true;
						} else {
							return false;
						}
					});

			return node;
		};

		my.getLinkOf = function(sourceNode, targetNode) {

			var link = _.find(my.links,function(l) {
						if (l.source === sourceNode && l.target === targetNode) {
							return true;
						} else {
							return false;
						}
					});

			return link;
		};

		that.getHtmlContentForRedirect = function(theTitle) {

			$.ajax({
				type : 'POST',
				url : 'http://en.wikipedia.org/w/api.php?action=parse&page='+ encodeURI(theTitle) + '&redirects=true',
				data: {
					format: 'json'
				},
				dataType: 'jsonp',
				async: false,
				success : function(data) {
					that.setCurrentNodeFromOutNodes(data.parse.title); 
				}
			});
		};

		that.getHtmlContentFor = function(htmlElement,theNode) {

			var theTitle = theNode.title();
			var htmlContent = theNode.htmlContent();
			
			if(htmlContent !== null) {

				console.log("htmlElement from CACHE");

				$(htmlElement).html(htmlContent);
				
			} else {

				$.ajax({
	        		type : 'POST',
	        		url : 'http://en.wikipedia.org/w/api.php?action=parse&page='+ encodeURI(theTitle) + '&prop=text',
	        		data: {
				        format: 'json'
				    },
				    dataType: 'jsonp',
	        		async: false,
	        		success : function(data) {
	        			var htmlTitle = "<h1><strong>" + data.parse.title + "</strong></h1>";
	        			var htmlRawContent = (htmlTitle + data.parse.text["*"]);
	        			var myFragment = new WTextFormatter();
	        			var htmlCont = myFragment.formatHtmlContent(htmlRawContent);
	        			theNode.htmlContent(htmlCont);
	        			$(htmlElement).html(htmlCont);
	        		}
	    		});
			}	
		};

		that.pathFinder = function(startNodeTitle, endNodeTitle) {

			$.ajax({
        		type : 'GET',
        		url : '/pathFinderResult/' + startNodeTitle + '/' + endNodeTitle,
        		async: false,
        		success : function(data) { 
        			console.log("pathFinder data", data);
        			my.addPathSegment(data);
        		}
    		});
		};

		that.getNewSearch = function(theTitle) {

			$.ajax({
		        type : 'GET',
		        url : '/searchResult/' + theTitle,
		        success : function(data) { 
		   			my.addPathSingle(data);
		        }
    		});

		};

		that.setHoverToBranch = function(theTitle) {
			var hoverNode = _.find(that.nodes(),function(n) {
				if (n.title() === theTitle) {
					return true;
				} else {
					return false;
				}
			});
			my.setHover(hoverNode);
		};

		my.setHover = function(theNode) {
			theNode.hover(true);
			if (theNode.depth() > 0) {
				my.setHover(theNode.parentWNode());
			}
		};

		that.setUnhover = function() {
			$.each(that.nodes(),function(i,n) {
				n.hover(false);
			});
		};

		///////////// GETTERS & SETTERS
		that.model = function(theModel) {
			if (theModel === undefined) {
				return my.model;
			} else {
				my.model = theModel;
			}
		};

		that.nodes = function() {

			return my.nodes;

		};

		that.links = function() {

			return my.links;
			
		};

		that.currentNode = function() {

			return my.currentNode;
			
		};

		that.relevantNodeCount = function(theCount) {
			if (theCount === undefined) {
				return my.relevantNodeCount;
			} else {
				my.relevantNodeCount = theCount;
			}
		};



		
		///////////// BOILERPLATE for bekbónka
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		var WGModel = Backbone.Model.extend(constructor.apply(this,arguments));
		that = new WGModel();
		return that;
	};

	return WGraphModel;
});