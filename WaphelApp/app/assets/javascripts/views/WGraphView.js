define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WGraphModel',
	'../views/WSingleView',
	'../views/WTextView',
	'svgpan',
	'd3'
], function($,_,Backbone,WGraphModel,WSingleView,WTextView){

	var WGraphView = function () {
		var my = {}, that = {};

		that.el = $('#graphcontainer');


		////TODO: ellenöörizni!
		that.events = {"click svg g g " : "nodeClick", "mouseover svg g g circle" : "nodeHover","mouseout svg g g circle" : "nodeHoverOut"};

		my.width = 900, my.height = 400;
		my.forceMargin = 30;
		my.nodeMargin = 17;

		that.model = {};
		my.svg = {};
		my.force = {};
		my.nodeGroup = {};
		my.linkGroup = {};
		my.forceLinks = null;
		my.forceNodes = null;
		my.svgLinks = null;
		my.svgNodes = null;

		//isCollapsed = true;

		//my.currentID = "";
		//my.startID = "";

		that.create = function(theModel) {
			that.model = theModel;
			//my.currentID = that.model.currentNode().id();
			//my.startID = that.model.searchPath.startNode().id();
			_.bindAll(this);
			my.width = $('body').width() / 2;
			my.height = $('body').height() * 3 / 4;

			//console.log("width and height", my.width, my.height);

			my.svg = d3.select(that.el).append('svg')
				.attr("width",my.width)
				.attr("height",my.height);

			////TODO: ellenöörizni!
			that.listenTo(that.model, "change:graphModelChanged", that.update);

			///////////// d3 force
			my.startForce();
			my.forceNodes = my.force.nodes();
			my.forceLinks = my.force.links();
			my.svgNodes = my.svg.selectAll(".node");
			my.svgLinks = my.svg.selectAll(".link");
			////////////

			my.linkGroup = my.svg.append("g").attr("id","linkGroup");
			my.nodeGroup = my.svg.append("g").attr("id","nodeGroup");
			my.updateForceData();

		};

		my.startForce = function() {
			my.force = d3.layout.force()
				.size([my.width, my.height])
				.nodes(that.model.nodes())
				.links(that.model.links())
				.gravity(0.05)
				.alpha(0.5)
				.on("tick", my.tick);

			my.force.linkStrength(function(d) {
				if (d.source.isCurrentNode() || d.target.isCurrentNode()) {
					return 0.7;
				} else {
					return 1;
				}
			});

			my.force.linkDistance(function(d) {
				if (d.source.isCurrentNode() || d.target.isCurrentNode()) {
					return 130;
				} else if(d.target.belongsToSearchPath()){
					return 60;
				} else {
					return 120;
				}
			});

			my.force.charge(function (d) {
				return -500;
			});

			my.force.start();
		};

		my.updateForceData = function() {
			//my.currentID = that.model.searchPath.currentNode().id();
			my.svgNodes = my.nodeGroup.selectAll(".node");
			my.forceNodes = my.svgNodes.data(that.model.nodes());

			my.forceNodes
				.attr("id",function(d) {
					return d.id();
				})
				.attr("title",function(d){
					return d.title();
				})
				.attr("class", function(d) {
					if (d.belongsToSearchPath()) {
						//var id = d.id();
						if (d.isStartNode()) {
							return "node startNode";
						} else if (d.isCurrentNode()) {
							return "node currentNode";
						} else if (d.belongsToCurrentBranch()) {
							return "currentBranch node";
						} else {
							return "graphPath node";
						}
					} else {
						return "graph node";
					}
				});

			my.forceNodes.select("text").text(function(d) {
				return my.wordwrap(d.title(),20); 
			});

			//set initial values manually???
			var n = my.forceNodes.enter().append("g")
				.attr("id",function(d){
					return d.id();
				})
				.attr("title",function(d){
					return d.title();
				})
				.attr("class", function(d) {
					if (d.belongsToSearchPath()) {
						//var id = d.id();
						if (d.isStartNode()) {
							return "node startNode";
						} else if (d.isCurrentNode()) {
							return "node currentNode";
						} else if (d.belongsToCurrentBranch()) {
							return "currentBranch node";
						} else {
							return "graphPath node";
						}
					} else {
						return "graph node";
					}
				});

			n.append("rect").attr("opacity",0).attr("rx",3).attr("ry",3);

			n.append("text")
				.text(function(d) {
					return my.wordwrap(d.title(),20); 
				})
				.attr('x', 0)
				.attr('y', -15)
				.attr('y', -13)
				.attr("text-anchor", "middle");

			n.append("circle")
				.attr("r", function(d) {
					if (d.isCurrentNode()) {
						return 8;
					} else {
						return 3;
					}
				})
				.attr("cx",0)
				.attr("cy",0);

			my.forceNodes.exit().remove();

			my.svgLinks = my.linkGroup.selectAll(".link");
			my.forceLinks = my.svgLinks.data(that.model.links());

			my.forceLinks
				//.attr("id",function(d) {
				//	return d.id();
				//})
				.attr("class",function(d){			
					if (d.target.belongsToSearchPath()) {
						if (d.belongsToCurrentBranch()){
							return "currentBranch link";
						} else {
							return "graphPath link";
						}
					} else {
						return "graph link";
					}
				});

			var li = my.forceLinks.enter().append("g")
				.attr("class",function(d){			
					if (d.target.belongsToSearchPath()) {
						if (d.belongsToCurrentBranch()){
							//console.log("fölsfölamfa");
							return "currentBranch link";
						} else {
							return "graphPath link";
						}
					} else {
						return "graph link";
					}
				});
			li.append("path").attr("class","first");
			//li.append("path").attr("class","second");
		  
			my.forceLinks.exit().remove();
			my.startForce();
		};


		my.tick = function() {

			/*
			my.forceNodes
				.attr("transform", function(d){

				})*/
		/*
			my.forceNodes
				.attr("transform", function(d) {

				if(d.id() === my.currentID) {
					//d.x = (((my.width / 4) + d.x % (2 * my.width / 4)) - d.x) * 0.9 + d.x * 0.1;
					d.y = (my.height / 2 - d.y) * 0.05 + d.y;
					d.fixed = true;
					//d.y = (my.height / 4) + d.y % (2 * my.height / 4);
				
				} else {
					d.y = Math.max(my.forceMargin,Math.min(my.height-my.forceMargin,d.y));
					d.fixed = false;
					//d.y = Math.max(my.forceMargin,Math.min(my.height - my.forceMargin,d.y));
				}

				d.x = Math.max(my.forceMargin,Math.min(my.width - my.forceMargin,d.x));

				var tr = "translate(" + d.x + "," + d.y + ")";
				return tr; 
			});
		*/	
			///////TODO: each, properly!

			var hoverNode = $("#currentNodeHover");
			my.forceNodes[0].forEach(function(n,it) {
				var node = d3.select(n);
				var d = that.model.nodes()[it];

				if(d !== undefined) {
					d.x = Math.max(my.forceMargin,Math.min(my.width-my.forceMargin,d.x));
					d.y = Math.max(my.forceMargin,Math.min(my.height-my.forceMargin,d.y));
					var tr = "translate(" + d.x + "," + d.y + ")";
					
					node.attr("transform",tr);
					var title = node.attr("title");
					if (hoverNode !== null) {
						if ( title === hoverNode.attr("title")) {
							hoverNode.attr("transform",tr);
						}
					}

					//console.log("no", this.node[0][it].getBBox());
					//console.log(d3.select(this.node[0][it]).select("rect"));
					/*
					var textEl = node.select("text")[0][0];
					var bb = textEl.getBBox();
					var textBGRect = d3.select(node.select("rect")[0][0]);
					textBGRect.attr("x",bb.x - 10).attr("y",bb.y - 4).attr("width",bb.width + 20).attr("height",bb.height + 8);
					*/
				}	
			});
			
			///////TODO: each!
			/*
			my.forceLinks
				.attr("x1", function(d) {
					var sx = d.source.x;
					var sy = d.source.y;
					var tx = d.target.x;
					var ty = d.target.y;
					var x1, y1, x2, y2;
					var phi = Math.atan2(ty - sy, tx - sx);
					var sinPhi = Math.sin(phi);
					var cosPhi = Math.cos(phi);
					d.x1 = sx + my.nodeMargin * cosPhi;
					d.y1 = sy + my.nodeMargin * sinPhi;
					d.x2 = tx - my.nodeMargin * cosPhi;
					d.y2 = ty - my.nodeMargin * sinPhi;
					return d.x1;
				})
				.attr("y1", function(d) {
					return d.y1;
				})
				.attr("x2", function(d) {
					return d.x2;
				})
				.attr("y2", function(d) {
					return d.y2;
				});*/

			my.forceLinks.attr("transform",function(d) {
					var sx = d.source.x;
					var sy = d.source.y;
					var tx = d.target.x;
					var ty = d.target.y;
					var sw = d.strokeWidth();
					var phi = Math.atan2(ty - sy, tx - sx) / Math.PI * 180 ;
					d.len = Math.sqrt( (tx - sx) * (tx - sx) + (ty - sy) * (ty - sy));
					var tr ="translate(" + sx + "," +  (sy - sw/2) + ") rotate(" + phi + " " + 0 + " " + (sw/2) + ")";
					return tr;
				})
					.select("path")
					.attr("d",function(d) {
						return my.getSVGPath3(d.len,d.strokeWidth(),true);
					})
					.attr("class", function(d) {
						if (d.hover()) {
							return "hover";
						} else {
							return "unhover";
						}
					});
			
			/*svg path 3 uses only one!
			my.forceLinks.select(".second").attr("d",function(d) {
				return my.getSVGPath3(d.len,d.strokeWidth(),false);
			});
			*/
			
		};

		my.getSVGPath3 = function(w,h,first) {
			//w is width, h is height
			var m = 16; //margin
			var g = 2.5; //gap
			var ar = h*0.5; //"arrow"
			var s = 0; //"shift"
			if ((w-m*2) < 24) {
				g = (w-m*2)/12;
			}
			
			var r1 = h/2;
			var r0 = 2;

			return "M" + (m + r0) + "," + (h/2 + r0) + "A" + r0 + "," + r0 + ",180,0,1," + (m + r0) + "," + (h/2 - r0) + "L" + (w-m-r1) + "," + (h/2-r1) + " A" + r1 + "," + r1 + ",180,0,1," + (w-m-r1) + "," +(h/2+r1) + " L" +  (m + r0) + "," + (h/2+r0) + " Z";
		};


		my.getSVGPath2 = function(w,h,first) {
			//w is width, h is height
			var m = 16; //margin
			var g = 2.5; //gap
			var ar = h*0.5; //"arrow"
			var s = 0; //"shift"
			if ((w-m*2) < 24) {
				g = (w-m*2)/12;
			}

			if (first) {
				return "M" + m  + "," + h + "L" + m + ",0"  +  "L" + (w/2-g + s) + ",0 L" + (w/2-g + ar + s) + "," + (h/2) + " L" + (w/2-g + s) + "," + h + "Z";
			} else {
				return "M" + (w - (m + ar/2)) + "," + h + "L" + (w - (m - ar/2)) + "," + (h/2) +  "L" + (w - (m + ar/2)) + ",0 L" + (w/2 + s) + ",0 L" + (w/2 + ar + s) + "," + (h/2) + " L" + (w/2 + s) + "," + h + "Z";
			}
		};

		my.getSVGPath = function(w,h,first) {
			//w is width, h is height
			var m = 16; //margin
			var g = 2.5; //gap
			var ar = h*0.5; //"arrow"
			var s = 0; //"shift"
			if ((w-m*2) < 24) {
				g = (w-m*2)/12;
			}

			if (first) {
				return "M" + (m + h/2) + "," + h + "A" + (h/2) + "," + (h/2) + ",180,0,1," + (m + h/2) + ",0 L" + (w/2-g + s) + ",0 L" + (w/2-g + ar + s) + "," + (h/2) + " L" + (w/2-g + s) + "," + h + "Z";
			} else {
				return "M" + (w - (m + h/2)) + "," + h + "A" + (h/2) + "," + (h/2) + ",180,0,0," + (w - (m + h/2)) + ",0 L" + (w/2 + s) + ",0 L" + (w/2 + ar + s) + "," + (h/2) + " L" + (w/2 + s) + "," + h + "Z";
			}
		};

		that.nodeHover = function(evt) {
			d3.select("#currentNodeHover").remove();
			var eventTarget = d3.select(evt.currentTarget);
			var eventTargetParent = d3.select(evt.currentTarget.parentNode);
			var theTitle = eventTargetParent.attr("title");
			that.model.setHoverToBranch(theTitle);

			var currentNodeHoverElement = my.nodeGroup.append("g")
					.attr("id","currentNodeHover")
					.attr("title",theTitle)
					.attr("transform",eventTargetParent.attr("transform"))
					.attr("opacity",0);

			var textEl = eventTargetParent.select("text")[0][0];
			var bb = textEl.getBBox();
			currentNodeHoverElement.transition().attr("opacity",1);
			currentNodeHoverElement.append("rect")
					.attr("x",bb.x - 10)
					.attr("y",bb.y - 4)
					.attr("width",bb.width + 20)
					.attr("height",bb.height + 8)
					.attr("rx",3)
					.attr("ry",3)
					.attr("fill","#fa3219");
			currentNodeHoverElement.append("text")
					.text(textEl.textContent)
					.attr('x', 0)
					.attr('y', -15)
					.attr('y', -13)
					.attr("fill", "#eeeeee")
					.attr("text-anchor", "middle");

			my.forceLinks.select("path").attr("class", function(d) {
						if (d.hover()) {
							return "hover";
						} else {
							return "unhover";
						}
					});
		};

		that.nodeHoverOut = function(evt) {
			d3.select("#currentNodeHover").transition().attr("opacity",0);
			that.model.setUnhover();
			my.forceLinks.select("path").attr("class", function(d) {
				if (d.hover()) {
					return "hover";
				} else {
					return "unhover";
				}
			});
		};

		that.nodeClick = function(evt) {
			if (that.model) {
				that.model.updateCurrentPath($(evt.currentTarget).attr("title"));
			} else {
				console.error("model error !!!");
			}
			d3.select("#currentNodeHover").transition().attr("opacity",0);
		};

		//using this: http://james.padolsey.com/javascript/wordwrap-for-javascript/
		my.wordwrap = function( str, width, brk, cut ) {
		    brk = brk || '\n';
		    width = width || 75;
		    cut = cut || false;
		 
		    if (!str) { return str; }
		 
		    var regex = '.{1,' +width+ '}(\\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\\S+?(\\s|$)');
		 	var result = str.match( RegExp(regex, 'g') ).join( brk );
		    return result;
		};
		
		//TODO setTimeOut
		that.update = function() {
			//console.log("update triggered");
				my.force.stop();
				my.updateForceData();
			
		};

		//////////////// BOILERPLATE
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		_.extend(that, Backbone.Events);
		var WGView = Backbone.View.extend(constructor.apply(this,arguments));
		that = new WGView();
		return that;
	};

	return WGraphView;
});