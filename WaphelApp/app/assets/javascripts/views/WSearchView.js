define([
	'jquery',
	'jqueryui',
	'underscore',
	'backbone',
	'../models/WGraph',
	'../views/WSingleView',
	'../views/WGraphView',
	'svgpan',
	'd3'
], function($,jqueryui,_,Backbone,WGraph,WSingleView,WGraphView){

	var WSearchView = function() {

		var my = {}, that = {};

		that.el = $('#wsearchform');

		that.events = {

			"click #button" : "buttonClick",
			"click #buttonOneSearch" : "buttonOneSearchClick",
			"click #buttonTwoSearch" : "buttonTwoSearchClick"
		};

		my.startNodeTitle = undefined;
		my.endNodeTitle = undefined;


		that.create =  function(theModel) {

			my.model = theModel;
			_.bindAll(this);
			//that.listenTo(my.model, "change:graphModelChanged", my.update);
		};

		that.buttonClick = function(evt) {

			my.startNodeTitle = $('#startNode').val();
			my.endNodeTitle = $('#endNode').val();

			if( (my.startNodeTitle !== undefined && my.startNodeTitle.length > 0) && (my.endNodeTitle !== undefined && my.endNodeTitle.length > 0)) {
				my.model.pathFinder(my.startNodeTitle,my.endNodeTitle);
			} else {

				alert("Oops! Something went wrong. Please type the titles again.");
			}

		};

		that.buttonOneSearchClick = function(evt) {

			my.startNodeTitle = $('#startNode').val();

			if(my.startNodeTitle !== undefined && my.startNodeTitle.length > 0) {
				my.model.getNewSearch(my.startNodeTitle);
			} else {

				alert("Oops! Something went wrong. Please type the titles again.");
			}

		};

		that.buttonTwoSearchClick = function(evt) {

			my.endNodeTitle = $('#endNode').val();

			if(my.endNodeTitle !== undefined && my.endNodeTitle.length > 0) {
				my.model.getNewSearch(my.endNodeTitle);
			} else {

				alert("Oops! Something went wrong. Please type the titles again.");
			}

		};

		that.autocomplete = function() {

			$( "#startNode" ).autocomplete({
              source: function( request, response ) {
                $.ajax({
                  url: "http://en.wikipedia.org/w/api.php?action=opensearch&search=" + request.term + "&limit=10&namespace=0&format=json",
                  dataType: "jsonp",
                  data: {
                    format: "json"
                  },
                  success: function( data ) {
                      response(data["1"]);
                  }
                });
              },
              minLength: 2
            });

			$( "#endNode" ).autocomplete({
              source: function( request, response ) {
                $.ajax({
                  url: "http://en.wikipedia.org/w/api.php?action=opensearch&search=" + request.term + "&limit=10&namespace=0&format=json",
                  dataType: "jsonp",
                  data: {
                    format: "json"
                  },
                  success: function( data ) {
                      response(data["1"]);
                  }
                });
              },
              minLength: 2
	        });
		};

		
	
		//////////////// BOILERPLATE
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		_.extend(that, Backbone.Events);
		var WSFView = Backbone.View.extend(constructor.apply(this,arguments));
		that = new WSFView();
		return that;

		
	};

	return WSearchView;
});