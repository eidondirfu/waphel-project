define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WNode',
	'd3'
], function($,_,Backbone,WNode){

	var WSingleView = function () {
		var my = {}, that = {};

		my.jq = function (input) {
			return input.replace(/\W/g, '');
		};

		that.el = $('#radarcontainer');

		that.events = {"click g #nodes g " : "outNodeClick"};

		that.collapseDuration = 300;
		that.expandDuration = 300;


		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
			};



		that.create = function(theModel, theMinRadius, theMargin) {

			_.bindAll(this);
			my.model = theModel;
			that.listenTo(my.model, "change:graphModelChanged", my.update);
			var theNode = my.model.currentNode();
			my.setup(theNode, theMinRadius, theMargin);

		};

		my.setup = function(theNode, theMinRadius, theMargin) {

			my.currentNode = theNode;
			my.nodeCount = theNode.outNodes().length;

			that.width = $('body').width() / 2;
			that.height = $('body').height();
			that.x = that.width / 2;
			that.y = that.height / 2;
			my.totalSize = 15 * my.nodeCount;

			//my.r = 0;
			//my.graph = graph;

			my.minRadius = theMinRadius;
			///OVERRIDE !!!!
			my.maxRadius = that.width * 0.45 - theMargin;

			my.defaultMargin = theMargin;
			my.defaultMinRadius = theMinRadius;


			my.intervalSize = Math.max(0,my.maxRadius - my.minRadius);
			my.visibleInterval = [0,my.intervalSize];

			//my.phi = (34.111111111 / 180) * Math.PI;
			//my.phi = Math.PI * 2 / 13.78;
			my.phi = Math.PI * 2 / 15;
			my.collapsedRadius = 7;

			//my.normalizedDistances = [];
			//my.relevancesAll = [];

			my.isoRadii = [];
			my.isoInterval = 0.01;

			// TODO define it more precisely depending on dpi of the screen
			my.prevZoom = 1;
			my.zoomFactor = 1/10;

			that.svg = d3.select(that.el).append('svg');

			that.svg
				.attr("width",that.width)
				.attr("height", that.height);

			my.scale = d3.scale.linear();
			my.scale.domain([0,my.intervalSize]);
			my.scale.range([my.minRadius, my.maxRadius]);

			//console.log(my);
			//debugger
			my.computeIsoRadii();

			for (var it=1; it < my.nodeCount; it++) {
				my.currentNode.outNodes()[it].node.lambda(my.phi * it % (Math.PI * 2));
			}

			my.group = that.svg
				//.append('svg:g')	            
				.call(d3.behavior.zoom().on("zoom", function(){
					//console.log(d3.event);
					my.zoom(d3.event);
				}))
				.insert("g","*")
				.attr("id","singleView");
				//.attr("transform","translate(" + that.x + "," + that.y + "),scale(0.1)");
			

			my.bgCircle = my.group.append('circle')
				.attr('cx', that.x)
				.attr('cy', that.y)
				.attr('r', my.maxRadius)
				.attr("stroke", "grey")
				.attr("stroke-width", 5)
				.attr('fill', "none")
				.attr('opacity', 1);
			
			my.bgCircle.call(d3.behavior.zoom().on("zoom", function () {my.zoom(d3.event);}));

			var isoCircles = my.group.append('g').attr("id","isoCircles").attr("opacity",1);  

			my.nodes = my.group.append('g').attr("id","nodes").attr("opacity",1);  

			var node = my.nodes.selectAll(".WSnode")
				.data(_.pluck(my.currentNode.outNodes(),"node"))
				.enter().append("g")
				.attr("class", "WSnode")
				.attr("id",function(d,i){
					return  my.jq(my.currentNode.title()) + my.jq(d.title()) + "Node";
					//return "id" + i;
				})
				.attr("opacity",1)
				.attr("transform",function(d,i){
					var x = that.x;
					var y = that.y;
					if(i !== 0) {
						x = that.x + 5 * i * Math.cos(i*my.phi);
						y = that.y + 5 * i * Math.sin(i*my.phi);
						//x = width/2 + 1000*(1 - d.relevance) * Math.cos(i*phi);
						//y = height/2 + 1000*(1 - d.relevance) * Math.sin(i*phi);
					} 
					return "translate(" + x + "," + y + ")";
				});
					//.call(force.drag);

			node.append("text")
				.text(function(d) {return d.title()})
				.attr('x', function (d) {
					if (d.lambda() > Math.PI/2 + my.phi/2 && d.lambda() <= Math.PI*3/2 + my.phi/2) {
						return -8;
					} else {
						return 8;
					}
				})
				.attr('y', 4)
				//.attr("fill","white")
				.attr("text-anchor",function (d) {
					if (d.lambda() > Math.PI/2 + my.phi/2 && d.lambda() <= Math.PI*3/2 + my.phi/2) {
						return "end";
					} else {
						return "start";
					}
				});
			
			node.append("circle")
				.attr("cx",0) ///OR 1???
				.attr("cy",0)
				.attr("r", 5);
				//.attr("fill","white");

			my.singleNode = my.group.append("g")
				.attr("transform",function() {
					return "translate(" + that.x + "," + that.y + ")";
				})
				.attr("id","singleNode");

			my.singleNode.append("circle")
				.attr("cx",0)
				.attr("cy",0)
				.attr("r",my.minRadius)
				.attr("fill","black")
				.attr("stroke",d3.rgb(250,50,25))
				.attr("stroke-width",15);

			my.singleNode.append("text")			
				.text(function () {return my.currentNode.title()})
				.attr('x', 0)
				.attr('y', 0)
				.attr("fill", "white")
				.attr("text-anchor", "middle");



			my.draw();
			//my.collapse();
			//setTimeout(function() {
			//	my.expand();	
			//}, 3000);
			
		};

		my.update = function() {

			//$(this).unbind()

			//console.log("single view update");

			$(that.el).empty();//that.svg.remove();

			my.setup(my.model.currentNode(),my.defaultMinRadius,my.defaultMargin);

		};

		that.outNodeClick = function(evt) {

			console.log("singleView outNodeClick", evt.currentTarget, $(evt.currentTarget).text());

			my.model.setCurrentNodeFromOutNodes($(evt.currentTarget).text());
		};

		that.expand = function() {
			var delay = 100;
			d3.select("#singleNode circle").transition().attr("r",my.maxRadius).duration(delay).ease("sin");

			d3.select("#singleNode circle").transition().attr("r",my.minRadius).duration(200).ease("sin").delay(delay);
			d3.select("#nodes").transition().attr("opacity",1).duration(0).delay(delay);
			d3.select("#isoCircles").transition().attr("opacity",1).duration(0).delay(delay);
			my.bgCircle.transition().attr("opacity",1).duration(0).delay(delay);

		};

		that.collapse = function() {
			var delay = 100;

			d3.select("#singleNode circle").transition().attr("r",my.maxRadius).duration(delay).ease("sin");

			d3.select("#singleNode circle").transition().attr("r",my.collapsedRadius).duration(200).ease("sin").delay(delay);
			d3.select("#nodes").transition().attr("opacity",0).duration(0).delay(delay);
			d3.select("#isoCircles").transition().attr("opacity",0).duration(0).delay(delay);
			my.bgCircle.transition().attr("opacity",0).duration(0).delay(delay);
		};

		my.computeIsoRadii = function() {
			//console.log("my.nodeCount", my.nodeCount);

			var ceilNumber = Math.ceil(1/my.isoInterval);
			//var ceilIndex = 0;

			for(var i = 0; i < my.nodeCount; i++) {
				var rel = my.currentNode.outNodes()[i].relevance;
				var nextCeil = Math.ceil(rel/my.isoInterval);
				var difference = ceilNumber - nextCeil;

				/**
				*TODO weights for radius instead of arithmetic mean
				**/
				if (i > 0) {
					while(difference > 0) {
						var radius = (my.currentNode.outNodes()[i].normalizedIndex + my.currentNode.outNodes()[i-1].normalizedIndex)/2;
						my.isoRadii.push(radius);
						difference--;
					}
				}
				ceilNumber = nextCeil;
			}
		};

		my.drawIsoCircle = function(radius,fillColor,opacity) {
			d3.select("#isoCircles")
				.append("circle")
				.attr("cx",that.x)
				.attr("cy",that.y)
				.attr("r",radius)
				.attr("fill",fillColor)
				//.attr("fill","none")
				//.attr("opacity", opacity)
				.attr("stroke", "white")
				.attr("stroke-opacity", 0.3)
				.attr("stroke-width", 1.5);  
		};

		//the REAL ONE!
		my.draw = function() {

			my.drawIsoCircles();

			for(var it=0; it < my.nodeCount; it++){
				
				var nodeRadius = my.currentNode.outNodes()[it].normalizedIndex*my.totalSize;
				//var lambda = (my.phi * it) % (Math.PI * 0.9) + (Math.PI * 1.1) / 2;
				//var n = 
				var lambda = my.phi * it;
				var title = my.currentNode.outNodes()[it].node.title();
				var id = my.jq(my.currentNode.title()) + my.jq(title) + "Node";
				//var id = "#id" + it;
				var node = d3.select("#" + id);
				//var node = my.svg.select(id);
				//var node = my.svg.select("#" + id);	            

				var x,y,r;
				if(nodeRadius <= my.visibleInterval[1] && nodeRadius >= my.visibleInterval[0]) {
					r = nodeRadius - my.visibleInterval[0];
					x = that.x + my.scale(r) * Math.cos(lambda);
					y = that.y + my.scale(r) * Math.sin(lambda);
					//console.log(lambda + " " + x + " " + y);
					node.attr("opacity",1.0);
				} else {
					node.attr("opacity",0.0);
					if (nodeRadius > my.visibleInterval[1]) {
						r = my.maxRadius;
					} else {
						r = my.minRadius;
					}
					x = that.x + r * Math.cos(lambda);
					y = that.y + r * Math.sin(lambda);
				}
				node.attr("transform","translate(" + x + "," + y + ")")
								
			}
		};

		my.drawIsoCircles = function() {
			$("#isoCircles *").remove();

			var bgCircleDrawn = false;
			var noneDrawn = true;
			var lastIndex = my.isoRadii.length - 1;

			for(var iter = my.isoRadii.length - 1; iter > 0; iter--) {
				var radius = my.isoRadii[iter]*my.totalSize;

				if(radius >= my.visibleInterval[1]) {
					lastIndex = iter;
				}

				if((radius <= my.visibleInterval[1] && radius >= my.visibleInterval[0]) && (my.visibleInterval[1] != my.visibleInterval[0])) {

					noneDrawn = false;                

					if (!bgCircleDrawn) {
						var ratio;
						if (iter + 1 == my.isoRadii.length) {
							ratio = 1;
						} else {
							ratio = my.isoRadii[iter + 1];   
						}
						var color = my.getIsoFillColor(ratio);
						var opacity = 1;
						my.drawIsoCircle(my.maxRadius, color, opacity);
						bgCircleDrawn = true;
					}

					var rr = my.scale(radius - my.visibleInterval[0]);
					var inc = radius/my.totalSize;
					var color = my.getIsoFillColor(inc);
					var opacity = 1;
					my.drawIsoCircle(rr,color,opacity);
				}
			}

			if (noneDrawn) {
				console.log("none drawn!" + lastIndex);
				var ratio = my.isoRadii[lastIndex];
				var color = my.getIsoFillColor(ratio);
				my.drawIsoCircle(my.maxRadius,color);
			}
		};

		my.getIsoFillColor = function(ratio) {
			var r0 = 255, 
				r1 = 174, 
				g0 = 149, 
				g1 = 192,
				b0 = 149, 
				b1 = 255;
				return d3.rgb(r0*(1-ratio) + r1*ratio, g0*(1-ratio) + g1*ratio, b0*(1-ratio) + b1*ratio);		
		};

		my.getOpacity = function(ratio) {
				var a0 = 1,	a1 = 0;
				return a0*(1-ratio) + a1*ratio;
		};

		my.zoom = function(evt) {

			//console.log(evt);

			if (evt.sourceEvent.type === "mousewheel") {
				//var nextZoom = evt.scale;
				//var zoom = nextZoom - prevZoom;
				//prevZoom = nextZoom;
				var zoom = evt.sourceEvent.wheelDeltaY * my.zoomFactor;

				if(zoom + my.visibleInterval[0] < 0) {
					zoom = -my.visibleInterval[0];
				} else if(zoom + my.visibleInterval[1] > my.totalSize) {
					zoom = my.totalSize - my.visibleInterval[1];
				}
				my.visibleInterval[0] += zoom;
				my.visibleInterval[1] += zoom;
				my.draw();
			}
		};
					

		_.extend(that, Backbone.Events);
		var WSView = Backbone.View.extend(constructor.apply(this,arguments));
		that = new WSView();
		return that;
	};

	return WSingleView;
});