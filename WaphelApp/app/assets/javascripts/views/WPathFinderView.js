define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WGraph',
	'../views/WSingleView',
	'../views/WGraphView',
	'svgpan',
	'd3'
], function($,_,Backbone,WGraph,WSingleView,WGraphView){

	var WPathFinderView = function() {

		var my = {}, that = {};

		that.el = $('#pathfindercontainer');

		//that.events = {"click a" : "linkClick"};
		my.width = 900, my.height = 400;
		my.forceMargin = 30;

		my.model = {};
		my.svg = {};
		my.force = {};
		my.nodeGroup = {};
		my.linkGroup = {};
		my.forceLinks = null;
		my.forceNodes = null;
		my.svgLinks = null;
		my.svgNodes = null;


		that.create = function(theModel) {
			my.model = theModel;
			_.bindAll(this);
			my.width = $('body').width() / 2;
			my.height = $('body').height();

			//console.log("width and height", my.width, my.height);

			my.svg = d3.select(that.el).append('svg')
				.attr("width",my.width)
				.attr("height",my.height);

			////TODO: ellenöörizni!
			that.listenTo(my.model, "change:pathFinderModelChanged", that.update);

			///////////// d3 force
			my.startForce();
			my.forceNodes = my.force.nodes();
			my.forceLinks = my.force.links();
			my.svgNodes = my.svg.selectAll(".node");
			my.svgLinks = my.svg.selectAll(".link");

			console.log("force path nodes and links", my.forceNodes, my.forceLinks);
			////////////

			my.linkGroup = my.svg.append("g").attr("id","linkGroup");
			my.nodeGroup = my.svg.append("g").attr("id","nodeGroup");
			my.updateForceData();

		};


		my.startForce = function() {
			my.force = d3.layout.force()
				.size([my.width, my.height])
				.nodes(my.model.nodes())
				.links(my.model.links())
				.gravity(0.05)
				.alpha(0.5)
				.on("tick", my.tick);

			my.force.linkStrength(function(d) {
				
					return 1;
				
			});

			my.force.linkDistance(function(d) {
				
					return 120;
				
			});

			my.force.charge(function (d) {
				return -500;
			});

			my.force.start();
		};

		my.updateForceData = function() {
			//my.currentID = that.model.currentNode().id();
			my.svgNodes = my.nodeGroup.selectAll(".node");
			my.forceNodes = my.svgNodes.data(my.model.nodes());

			my.forceNodes
				.attr("title",function(d){
					return d.title();
				})
				.attr("class", function(d) {
					
					return "graphPath node";
						
				});

			my.forceNodes.select("text").text(function(d) {
				return d.title(); 
			});

			//set initial values manually???
			var n = my.forceNodes.enter().append("g")
				.attr("title",function(d){
					return d.title();
				})
				.attr("class", function(d) {
					
					return "graphPath node";
				});

			n.append("rect").attr("opacity",0).attr("rx",3).attr("ry",3);

			n.append("text")
				.text(function(d) {return d.title(); })
				.attr('x', 0)
				.attr('y', -15)
				.attr('y', -13)
				.attr("text-anchor", "middle");

			n.append("circle")
				.attr("r", function(d) {
					return 3;
				})
				.attr("cx",0)
				.attr("cy",0);

			my.forceNodes.exit().remove();

			my.svgLinks = my.linkGroup.selectAll(".link");
			my.forceLinks = my.svgLinks.data(my.model.links());

			my.forceLinks
				//.attr("id",function(d) {
				//	return d.id();
				//})
				.attr("class",function(d){			
						
					return "graphPath link";
				});

			var li = my.forceLinks.enter().append("g")
				.attr("class",function(d){			
					return "graphPath link";
				});
			li.append("path").attr("class","first");
			li.append("path").attr("class","second");
		  
			my.forceLinks.exit().remove();
			my.startForce();
		};


		my.tick = function() {

			/*
			my.forceNodes
				.attr("transform", function(d){

				})*/
		/*
			my.forceNodes
				.attr("transform", function(d) {

				if(d.id() === my.currentID) {
					//d.x = (((my.width / 4) + d.x % (2 * my.width / 4)) - d.x) * 0.9 + d.x * 0.1;
					d.y = (my.height / 2 - d.y) * 0.05 + d.y;
					d.fixed = true;
					//d.y = (my.height / 4) + d.y % (2 * my.height / 4);
				
				} else {
					d.y = Math.max(my.forceMargin,Math.min(my.height-my.forceMargin,d.y));
					d.fixed = false;
					//d.y = Math.max(my.forceMargin,Math.min(my.height - my.forceMargin,d.y));
				}

				d.x = Math.max(my.forceMargin,Math.min(my.width - my.forceMargin,d.x));

				var tr = "translate(" + d.x + "," + d.y + ")";
				return tr; 
			});
		*/	
			///////TODO: each, properly!

			my.forceNodes[0].forEach(function(n,it) {
				var node = d3.select(n);
				var d = my.model.nodes()[it];

				if(d !== undefined) {
					d.x = Math.max(my.forceMargin,Math.min(my.width-my.forceMargin,d.x));
					d.y = Math.max(my.forceMargin,Math.min(my.height-my.forceMargin,d.y));
					var tr = "translate(" + d.x + "," + d.y + ")";

					node.attr("transform",tr);
					//console.log("no", this.node[0][it].getBBox());
					//console.log(d3.select(this.node[0][it]).select("rect"));
					var textEl = node.select("text")[0][0];
					var bb = textEl.getBBox();
					var textBGRect = d3.select(node.select("rect")[0][0]);
					textBGRect.attr("x",bb.x - 10).attr("y",bb.y - 4).attr("width",bb.width + 20).attr("height",bb.height + 8);
				}	
			});
			
			///////TODO: each!
			/*
			my.forceLinks
				.attr("x1", function(d) {
					var sx = d.source.x;
					var sy = d.source.y;
					var tx = d.target.x;
					var ty = d.target.y;
					var x1, y1, x2, y2;
					var phi = Math.atan2(ty - sy, tx - sx);
					var sinPhi = Math.sin(phi);
					var cosPhi = Math.cos(phi);
					d.x1 = sx + my.nodeMargin * cosPhi;
					d.y1 = sy + my.nodeMargin * sinPhi;
					d.x2 = tx - my.nodeMargin * cosPhi;
					d.y2 = ty - my.nodeMargin * sinPhi;
					return d.x1;
				})
				.attr("y1", function(d) {
					return d.y1;
				})
				.attr("x2", function(d) {
					return d.x2;
				})
				.attr("y2", function(d) {
					return d.y2;
				});*/

			my.forceLinks.attr("transform",function(d) {
					var sx = d.source.x;
					var sy = d.source.y;
					var tx = d.target.x;
					var ty = d.target.y;
					var sw = d.strokeWidth();
					var phi = Math.atan2(ty - sy, tx - sx) / Math.PI * 180 ;
					d.len = Math.sqrt( (tx - sx) * (tx - sx) + (ty - sy) * (ty - sy));
					var tr ="translate(" + sx + "," +  (sy - sw/2) + ") rotate(" + phi + " " + 0 + " " + (sw/2) + ")";
					return tr;
			});

			my.forceLinks.select(".first").attr("d",function(d) {
				return my.getSVGPath(d.len,d.strokeWidth(),true);
			});
			
			my.forceLinks.select(".second").attr("d",function(d) {
				return my.getSVGPath(d.len,d.strokeWidth(),false);
			});
			
		};

		my.getSVGPath = function(w,h,first) {
			//w is width, h is height
			var m = 16; //margin
			var g = 2.5; //gap
			var ar = h*0.5; //"arrow"
			var s = 0; //"shift"
			if ((w-m*2) < 24) {
				g = (w-m*2)/12;
			}

			if (first) {
				return "M" + (m + h/2) + "," + h + "A" + (h/2) + "," + (h/2) + ",180,0,1," + (m + h/2) + ",0 L" + (w/2-g + s) + ",0 L" + (w/2-g + ar + s) + "," + (h/2) + " L" + (w/2-g + s) + "," + h + "Z";
			} else {
				return "M" + (w - (m + h/2)) + "," + h + "A" + (h/2) + "," + (h/2) + ",180,0,0," + (w - (m + h/2)) + ",0 L" + (w/2 + s) + ",0 L" + (w/2 + ar + s) + "," + (h/2) + " L" + (w/2 + s) + "," + h + "Z";
			}
		};

		that.update = function() {
			//console.log("update triggered");
				my.force.stop();
				my.updateForceData();
			
		};



		//////////////// BOILERPLATE
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		_.extend(that, Backbone.Events);
		var WPFView = Backbone.View.extend(constructor.apply(this,arguments));
		that = new WPFView();
		return that;

		
	};

	return WPathFinderView;
});