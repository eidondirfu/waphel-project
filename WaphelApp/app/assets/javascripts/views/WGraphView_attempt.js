define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WGraph',
	'd3'
], function($,_,Backbone,WGraph){

	var WGraphView = Backbone.View.extend({

		el : $('#container'),

		events: {"click svg g circle" : "nodeClick"},

		width : 960,

		height : 560,

		svg: {},
		links: null,
		nodes: null,
		link: null,
		node: null,

		currentID : "",

		initialize: function() {
			console.log(this);
			_.bindAll(this);
			this.svg = d3.select(this.el).append('svg')
				.attr("viewBox","0 0 " + this.width + " " + this.height); 
			
			this.listenTo(this.model, "change:currentNode", this.update);
			
			/*
			this.model.on('change', function() {
				console.log("change registered.");
				this.update();
			}, this);
			*/
			//this.on('change:currentNode',this.update);

			console.log("nodes",this.model.nodes);
			console.log("links",this.model.links);
			
			///////////// d3 force
			this.initForce(this);
			this.nodes = this.force.nodes();
			this.links = this.force.links();
			this.node = this.svg.selectAll(".node");
			this.link = this.svg.selectAll(".link");
			////////////

			this.restart(this);
		},

		restart : function(that) {
		  //this.link = this.link.data(this.links);
		  
		  //this.links[5].target.belongsToSearchPath = true;

		  //my.link.enter().insert("line", ".node")
		  console.log("view nodes", this.model.nodes);



		  this.nodes = this.svg.selectAll(".node");
  		  this.node = this.nodes.data(this.model.nodes);

  		  this.node.select("circle")
  		  	.attr("id",function(d) {
  				return d.id;
  		  	})
  		  	.attr("fill", function(d) {
  		  		if (d.belongsToSearchPath) {
  		  			//d.fixed = true;
  		  			return "black";
  		  		} else {
  		  			//d.fixed = false;
  		  			return "grey";
  		  		}
  		  	});

  		  this.node.select("text").text(function(d) {
  		  	console.log("updating", this);
  		  	return d.title; 
  		  });

  		  /*
  		  this.node.select("g").attr("transform", function(d) {
  		  	var tr = "translate(" + d.x + "," + d.y + ")";
  		  	return tr; 
  		  });
			*/

		  var n = this.node.enter().append("g")
		    	.attr("class", "node");
				/*		    	
		    	.attr("transform", function(d) {
		    		var x0;
		    		var y0;
		    		if (d.parentNode) {
		    			x0 = d.parentNode.x + 2;
		    			y0 = d.parentNode.y + 2;
		    		} else {
		    			x0 = 480;
		    			y0 = 1;
		    		}
  		  			var tr = "translate(" + x0 + "," + y0 + ")";
  					return tr; 
  				});
				*/
		  		//.call(this.force.drag);

			n.append("text")
	        	.text(function(d) {return d.title; })
	        	.attr('x', 0)
	        	.attr('y', -10)
	        	.attr("text-anchor", "middle");

			n.append("circle")
				.attr("id",function(d){
					//return d.title;
					return d.id;
				})
				.attr("r", 5)
				.attr("cx",0)
				.attr("cy",0)
				.attr("fill",function(d){
		      	
		      	if (d.belongsToSearchPath) {
		      		return "black";
		      	} else {
		      		return "grey";
		      	}
				
			});

		    this.node.exit().remove();

		    this.links = this.svg.selectAll(".link");
			this.link = this.links.data(this.model.links);

			this.link
				.attr("id",function(d) {
					return d.id;
				})
				.attr("stroke", function(d) {
			      	if (d.target.belongsToSearchPath) {
			      		return "black";
			      	} else {
			      		return "grey";
			      	}
			    })
		      	.attr("stroke-width", function(d) {
			      	if (d.target.belongsToSearchPath) {
			      		return "2px";
			      	} else {
			      		return "1px";
			      	}
			    });

			this.link.enter().append("line")
		  		.attr("id",function(d) {
		  		return d.id;
		  	})

		  	.attr("stroke",function(d) {
		      	if (d.target.belongsToSearchPath) {
		      		return "black";
		      	} else {
		      		return "grey";
		      	}
		  	  })
		  	  .attr("fill", "none")
		  	  .attr("stroke-width",function(d) {
		      	if (d.target.belongsToSearchPath) {
		      		return "2px";
		      	} else {
		      		return "1px";
		      	}
		   
		  	  })
		  	  .attr("class", "link");
		  
		  	  this.link.exit().remove();
		      /*
		      .on("mouseover", mouseover)
      		  .on("mouseout", mouseout)
      		  .on("mousedown",function (d) {
      		  	my.mousedown(d);
      		  });
			  */
			  this.initForce(this);
		},

		initForce : function(that) {
			that.force = d3.layout.force()
				.size([that.width, that.height])
				.nodes(that.model.nodes)
				.links(that.model.links)
				//.linkDistance(120)
				//.charge(-240)
				.gravity(0.05)
				//.on("tick", this.tick);
				.on("tick", (function(that) {
					// console.log("that", that);
					// that.node.attr("transform", function(d) {
  		 //  				var tr = "translate(" + d.x + "," + d.y + ")";
  		 //  				//console.log("x ", d.x, "y ", d.y);
  			// 			return tr; 
  			// 		});

					// that.link.attr("x1", function(d) {return d.source.x; })
				 //    	.attr("y1", function(d) { return d.source.y; })
				 //    	.attr("x2", function(d) { 
				 //    		//console.log("target", d.target);
				 //    		return d.target.x; })
				 //    	.attr("y2", function(d) { return d.target.y; });
					that.tick(that);
				})(this));

			that.force.linkDistance(function(d) {
				if (d.source.id === that.currentID) {
					return 120;
				} else {
					return 80;
				}
			});

			that.force.charge(function (d) {
			  	if (d.belongsToSearchPath) {
			  		return -320;
			  	} else {
			  		return -320;
			  	}
			});

			that.force.start();
			for (var it = 0; it < 10; it++) {
				this.force.tick(this);
			}
			//that.force.stop	();
		},


		tick : function(that) {
			//debugger
			//console.log("that", that);
			//that.nodes = that.svg.selectAll(".node");
			//that.node = that.nodes.data(this.model.nodes);

			//that.links = that.svg.selectAll(".link");
			//that.link = that.links.data(this.model.links);

			var n = that.model.get("node");

			n.attr("transform", function(d) {
  		  		var tr = "translate(" + d.x + "," + d.y + ")";
  		  		//console.log("x ", d.x, "y ", d.y);
  				return tr; 
  			});

			that.model.get("link").attr("x1", function(d) {return d.source.x; })
		    	.attr("y1", function(d) { return d.source.y; })
		    	.attr("x2", function(d) { 
		    		//console.log("target", d.target);
		    		return d.target.x; })
		    	.attr("y2", function(d) { return d.target.y; });
		},


		nodeClick: function(evt) {
			console.log("currentTarget ", evt.currentTarget);
			//console.log(this);
			if (this.model) {
				//this.model.set({"relevantNodeCount":9});
				this.currentID = $(evt.currentTarget).attr("id");
				this.model.setCurrentNode(this.currentID);
			} else {
				console.error("model error !!!");
			}
		},
		
		
		update: function() {
			console.log("update called!");
			this.restart(this);
		}
	});

	return WGraphView;
});