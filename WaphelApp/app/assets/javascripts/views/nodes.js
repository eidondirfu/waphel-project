define([
	'jquery',
	'underscore',
	'backbone',
	'd3'
], function($,_,Backbone){

	var nodes = Backbone.View.extend({
		
		svg : {},

		el : $('#container'),

		events: {"click" : "click"},

		initialize: function() {
			_.bindAll(this);
			this.svg = d3.select(this.el).append('svg')
				.attr("viewBox","0 0 900 560"); 
			this.listenTo(this.model,'change',this.update);
			//this.events["click #" + this.model.get("id")] = this.click;
			//this.model.on('change', this.render, this);
			this.render();
		},

		click: function() {
			var gyuri = "Matolcsy Gyurko";
			if (this.model && this.model.get("title") === gyuri) {
				this.model.trigger('change');
			}
			this.model.set({"title":gyuri});
		},

		update: function() {
			var id = this.model.get("id"); 
			$("#" + id + " text").text(this.model.get("title"));
			var x0 = d3.select("#" + id + " circle").attr("cx");
			d3.select("#" + id + " circle")
				.transition()
				.duration(3000)
				.ease("elastic")
				.attr("cx", x0 + 20);
			d3.select("#" + id)
				.transition()
				.duration(3000)
				.ease("elastic")
				.attr("transform", "translate(450," + 200 + ")");
		},

		render: function(){
			var id = this.model.get("id"); 
			
			//$("#" + id).remove();

			var g = this.svg.append('g')
				.attr("transform","translate(450,280)")
				.attr("id",id);
			//var m = this.get("model");

			g.append("text")
				.text(this.model.get("title"))
				.attr('x', 0)
				.attr('y', -60)
				.attr("text-anchor", "middle");

			g.append("circle")
				.attr("r", 50)
				.attr("cx",0)
				.attr("cy",0)
				.attr("fill","black");
		} 
		
	});

	return nodes;
});