define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WGraph',
	'../views/WSingleView',
	'../views/WGraphView',
	'svgpan',
	'd3'
], function($,_,Backbone,WGraph,WSingleView,WGraphView){

	var WTextView = function() {

		var my = {}, that = {};

		that.el = $('#textcontainer');

		that.events = {"click a" : "linkClick"};


		that.create =  function(theModel) {

			my.model = theModel;
			_.bindAll(this);
			that.listenTo(my.model, "change:graphModelChanged", my.update);

			var currentNode = my.model.currentNode();
			my.model.getHtmlContentFor("#textcontainer",currentNode);
		};

		my.update = function() {

			$(that.el).empty();

			var currentNode = my.model.currentNode();
			my.model.getHtmlContentFor("#textcontainer",currentNode);
		};

		that.linkClick = function(evt) {

			if($(evt.target).attr('class') && $(evt.target).attr('class') === 'mw-redirect') {

				var theTitle = $(evt.target).attr('title');
				my.model.getHtmlContentForRedirect(theTitle);

			} else {

				var theTitle = $(evt.target).attr('title');
				my.model.setCurrentNodeFromOutNodes(theTitle);
			}
		};



		//////////////// BOILERPLATE
		var constructor = function(inheritor) {
			_.extend(that, inheritor);
			return that;
		};

		_.extend(that, Backbone.Events);
		var WTView = Backbone.View.extend(constructor.apply(this,arguments));
		that = new WTView();
		return that;

		
	};

	return WTextView;
});