// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		"underscore": {
			exports: '_'
		},
		"backbone": {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	},
	paths: {
		'jquery': 'libs/jquery/jquery',
		'jqueryui': 'libs/jquery/jqueryui',
		'jquerytokeninput' : 'libs/jquery/jquerytokeninput',
		'underscore': 'libs/underscore/underscore',
		'backbone': 'libs/backbone/backbone',
		'd3': 'libs/d3/d3.v3',
		'svgpan': 'libs/svgpan/jquery-svgpan',
		'json2html': 'libs/jquery.json2html/jquery.json2html',
		'fokus': 'libs/fokus/fokus',
		'bootstrap' : 'libs/bootstrap/bootstrap',
		'spin' : 'libs/spin/spin'
	}
});

require([
	'jquery',
	'underscore',
	'backbone',
	'd3',
	'App'
], function($,_, Backbone, d3, App){

	var app;
	$.ajax({
        type : 'GET',
        url : '/searchResult/' + myTitle,
        success : function(data) { 
   			app = new App(data);
   			app.car();
 
        }
    });
   }); 