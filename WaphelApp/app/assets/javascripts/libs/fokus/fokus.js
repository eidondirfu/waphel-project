/*!
 * Fokus 0.5
 * http://lab.hakim.se/fokus
 * MIT licensed
 *
 * Copyright (C) 2012 Hakim El Hattab, http://hakim.se
 */
 (function() {
    var f = 5;
    var r = 0.75;
    var w,
    v,
    t = 0,
    o,
    b = {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    },
    h = {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    };
    function c() {
        if (k() && !window.__fokused) {
            window.__fokused = true;
            w = document.createElement("canvas");
            v = w.getContext("2d");
            w.style.position = "fixed";
            w.style.left = 0;
            w.style.top = 0;
            w.style.zIndex = 2147483647;
            w.style.pointerEvents = "none";
            document.addEventListener("mousedown", m, false);
            document.addEventListener("keyup", d, false);
            document.addEventListener("scroll", l, false);
            document.addEventListener("DOMMouseScroll", l, false);
            window.addEventListener("resize", e, false);
            e();
        }
    }
    function k() {
        return !! ("addEventListener" in document && "pointerEvents" in document.body.style);
    }
    function s() {
        var x = u();
        v.clearRect(0, 0, w.width, w.height);
        v.fillStyle = "rgba( 0, 0, 0, " + t + " )";
        v.fillRect(0, 0, w.width, w.height);
        if (x) {
            if (t < 0.1) {
                h = b;
            } else {
                h.left += (b.left - h.left) * 0.18;
                h.top += (b.top - h.top) * 0.18;
                h.right += (b.right - h.right) * 0.18;
                h.bottom += (b.bottom - h.bottom) * 0.18;
            }
        }
        v.clearRect(h.left - window.scrollX - f, h.top - window.scrollY - f, (h.right - h.left) + (f * 2), (h.bottom - h.top) + (f * 2));
        if (x) {
            t += (r - t) * 0.08;
        } else {
            t = Math.max((t * 0.85) - 0.02, 0);
        }
        cancelAnimationFrame(o);
        if (x || t > 0) {
            if (!w.parentNode) {
                document.body.appendChild(w);
            }
            o = requestAnimationFrame(s);
        } else {
            document.body.removeChild(w);
        }
    }
    function q(A) {
        b = {
            left: Number.MAX_VALUE,
            top: Number.MAX_VALUE,
            right: 0,
            bottom: 0
        };
        var z = n();
        for (var C = 0, F = z.length; C < F; C++) {
            var B = z[C];
            if (B.nodeName === "#text" && B.nodeValue.trim()) {
                B = B.parentNode;
            }
            var E = a(B);
            var H = E.x,
            G = E.y,
            I = B.offsetWidth,
            D = B.offsetHeight;
            if (B && typeof H === "number" && typeof I === "number" && (I > 0 || D > 0) && !B.nodeName.match(/^br$/gi)) {
                b.left = Math.min(b.left, H);
                b.top = Math.min(b.top, G);
                b.right = Math.max(b.right, H + I);
                b.bottom = Math.max(b.bottom, G + D);
            }
        }
        if (A) {
            h = b;
        }
        if (u()) {
            s();
        }
    }
    function u() {
        return b.left < b.right && b.top < b.bottom;
    }
    function m(x) {
        if (x.which !== 3) {
            document.addEventListener("mousemove", p, false);
            document.addEventListener("mouseup", i, false);
            q();
        }
    }
    function p(x) {
        q();
    }
    function i(x) {
        document.removeEventListener("mousemove", p, false);
        document.removeEventListener("mouseup", i, false);
        setTimeout(q, 1);
    }
    function d(x) {
        q();
    }
    function l(x) {
        q(true);
    }
    function e(x) {
        w.width = window.innerWidth;
        w.height = window.innerHeight;
    }
    function n() {
        if (window.getSelection) {
            var x = window.getSelection();
            if (!x.isCollapsed) {
                return j(x.getRangeAt(0));
            }
        }
        return [];
    }
    function j(y) {
        var z = y.startContainer;
        var x = y.endContainer;
        if (z == x) {
            if (z.nodeName === "#text") {
                return [z.parentNode];
            }
            return [z];
        }
        var A = [];
        while (z && z != x) {
            A.push(z = g(z));
        }
        z = y.startContainer;
        while (z && z != y.commonAncestorContainer) {
            A.unshift(z);
            z = z.parentNode;
        }
        return A;
    }
    function g(x) {
        if (x.hasChildNodes()) {
            return x.firstChild;
        } else {
            while (x && !x.nextSibling) {
                x = x.parentNode;
            }
            if (!x) {
                return null;
            }
            return x.nextSibling;
        }
    }
    function a(A) {
        var z = document.documentElement.offsetLeft,
        B = document.documentElement.offsetTop;
        if (A.offsetParent) {
            do {
                z += A.offsetLeft;
                B += A.offsetTop;
            }
            while (A = A.offsetParent);
        }
        return {
            x: z,
            y: B
        };
    } (function() {
        var z = 0;
        var A = ["ms", "moz", "webkit", "o"];
        for (var y = 0; y < A.length && !window.requestAnimationFrame;++y) {
            window.requestAnimationFrame = window[A[y] + "RequestAnimationFrame"];
            window.cancelAnimationFrame = window[A[y] + "CancelAnimationFrame"] || window[A[y] + "CancelRequestAnimationFrame"];
        }
        if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = function(E, B) {
                var x = new Date().getTime();
                var C = Math.max(0, 16 - (x - z));
                var D = window.setTimeout(function() {
                    E(x + C);
                }, C);
                z = x + C;
                return D;
            };
        }
        if (!window.cancelAnimationFrame) {
            window.cancelAnimationFrame = function(x) {
                clearTimeout(x);
            };
        }
    } ());
    c();
})();