define	([
	'jquery',
	'underscore',
	'backbone',
	'bootstrap',
	'models/WNode',
	'models/WGraphModel',
	'views/WGraphView',
	'views/WSingleView',
	'views/WTextView',
	'views/WSearchView'
], function($,_,Backbone,bootstrap,WNode,WGraphModel,WGraphView,WSingleView,WTextView,WSearchView) {

	var App = function(data) {
		var my = {}, that = {};

		//my.dataLinks = data;

		that.graph = new WGraphModel();
		that.graph.create(data);

		that.graphview = new WGraphView();
		that.graphview.create(that.graph);

		that.textView = new WTextView();
		that.textView.create(that.graph);

		that.singleView = new WSingleView();
		that.singleView.create(that.graph,50,30);

		//that.WPathFinderModel = new WPathFinderModel();
		//that.WPathFinderModel.create();

		that.WSearchView = new WSearchView();
		that.WSearchView.create(that.graph);
		that.WSearchView.autocomplete();

		//that.WPathFinderView = new WPathFinderView();
		//that.WPathFinderView.create(that.WPathFinderModel);

		

		that.car = function() {
          	
          	$('#myCarousel').carousel('pause');
    	};

		return that;
	};

	return App;
});