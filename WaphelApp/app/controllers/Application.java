package controllers;

import play.*;
import play.mvc.*;
import play.data.*;

import play.mvc.Controller;
import play.mvc.Result;

import views.html.*;
import models.*;

import play.libs.Json;
import play.Routes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;


public class Application extends Controller {


 static DynamicForm myForm = new DynamicForm();



  
  public static Result index() {

    return ok(index.render(myForm));
  }

  public static Result searchResult() {

  	String title = myForm.bindFromRequest().get("pageTitle");

    return ok(searchResult.render(title));
  	/*
    List result = models.Neo4jGraphDatabase.getLinks(title);

  	return ok(searchResult.render(Json.stringify(Json.toJson(result))));

  	/*
  	ArrayList<ArrayList<String>> adjP = models.Neo4jGraphDatabase.getEdges(title).get(0);
    ArrayList<ArrayList<String>> adjPBack = models.Neo4jGraphDatabase.getEdges(title).get(1);

    return ok(searchResult.render(title, adjP, adjPBack));

    */

  }

  public static Result getLinks(String pageTitle) {

    List result = models.Neo4jGraphDatabase.getLinks(pageTitle);

    return ok(Json.toJson(result));


  }

  public static Result findPath(String startNodeTitle, String endNodeTitle) {

    List result = models.Neo4jGraphDatabase.findPath(startNodeTitle, endNodeTitle);

    if(result != null) {
      return ok(Json.toJson(result));
    } else {

      return ok("no result found with titles " + startNodeTitle + " and " + endNodeTitle);
    }

    
  }

}

