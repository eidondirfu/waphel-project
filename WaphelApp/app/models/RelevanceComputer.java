package models;


import java.util.ArrayList;

import org.neo4j.graphdb.Node;

public class RelevanceComputer {
	
	private NodePreparator nodeOne;
	private NodePreparator nodeTwo;
	
	ArrayList<Node> oneForthNodes;
	ArrayList<Node> oneBackNodes;
	ArrayList<Node> twoForthNodes;
	ArrayList<Node> twoBackNodes;
	
	int oneBackSize;
	int twoBackSize;
	int oneForthSize;
	int twoForthSize;
	
	int Waphel = 4141575;


	
	
	public RelevanceComputer(NodePreparator nodeOne, NodePreparator nodeTwo) {
		this.nodeOne = nodeOne;
		this.nodeTwo = nodeTwo;
		
		this.oneForthNodes = this.nodeOne.getForthNodes();
		this.oneBackNodes = this.nodeOne.getBackNodes();
		this.twoForthNodes = this.nodeTwo.getForthNodes();
		this.twoBackNodes = this.nodeTwo.getBackNodes();
		
		this.oneBackSize = this.nodeOne.getBackSize();
		this.twoBackSize = this.nodeTwo.getBackSize();
		this.oneForthSize = this.nodeOne.getForthSize();
		this.twoForthSize = this.nodeTwo.getForthSize();
	}

	protected double getRelevance() {
		double relevance = 0d;
		
		double pageRankOne = pageRank(oneBackNodes);
		double pageRankTwo = pageRank(twoBackNodes);
		
		double alphaPrime = ( pageRankOne + pageRankTwo ) / 2.0;
		double bothBackSize = (double)(oneBackSize + twoBackSize);
		

		/*
		 * missing parts of algorithm !!!
		 * get common Nodes before reatainAll
		 */
		ArrayList<Node> commonBack = new ArrayList<Node>();
		commonBack.addAll(oneBackNodes);
		ArrayList<Node> commonForth = new ArrayList<Node>();
		commonForth.addAll(oneForthNodes);
		
		
		ArrayList<Node> commonForthBackOneTwo = new ArrayList<Node>();
		commonForthBackOneTwo.addAll(twoBackNodes);
		ArrayList<Node> commonForthBackTwoOne = new ArrayList<Node>();
		commonForthBackTwoOne.addAll(oneBackNodes);
		
		commonBack.retainAll(twoBackNodes);
		commonForth.retainAll(twoForthNodes);
		
		commonForthBackOneTwo.retainAll(oneForthNodes);
		commonForthBackTwoOne.retainAll(twoForthNodes);
		

		
		
		int commonBackSize = commonBack.size();
		//int commonForthSize = commonForth.size();
		
		double alpha = ( alphaPrime * (bothBackSize - 1.0) ) / ( alphaPrime * (bothBackSize - 2.0) + bothBackSize );
		double hopRankOneTwo = ( 1.0 / (double)(oneForthSize) ) * ( 1 + pageHopRank(commonForthBackOneTwo) );
		
		double hopRankTwoOne;
		
		if(twoForthNodes.contains(nodeOne.getNodeOne())) {
			hopRankTwoOne = ( 1.0 / (double)(twoForthSize) ) * ( 1 + pageHopRank(commonForthBackTwoOne) );
		}
		else {
			hopRankTwoOne = ( 1.0 / (double)(twoForthSize) ) * ( pageHopRank(commonForthBackTwoOne) );
		}

		double beta = ( hopRankOneTwo + hopRankTwoOne ) / 2.0;
		
		
		
		
		double backRelevance = normalizedGoogleDistance((double)oneBackSize, (double) twoBackSize, (double) commonBackSize, (double)Waphel);
		//double forthRelevance = normalizedGoogleDistance((double)oneForthSize, (double)twoForthSize, (double)commonForthSize, (double)Waphel);
		double numerator = norm(commonForth);
		double forthRelevance = ( numerator / norm(oneForthNodes) ) * ( numerator / norm(twoForthNodes) );
		

		double firstRelevance =  ( (1.0 - alpha) * forthRelevance) + (alpha * backRelevance);
		
		relevance = firstRelevance + beta * (1.0 - firstRelevance);
		
		
		return relevance;
	}
		
	private double normalizedGoogleDistance(double oneBack, double twoBack, double commonBack, double Waphel) {
		
		double backrel = 0d;
		
		if(oneBack != 0 && twoBack != 0 && commonBack != 0) {
			
			backrel = ( Math.log(Math.max(oneBack,twoBack)) - Math.log(commonBack) ) / ( Math.log(Waphel) - Math.log(Math.min(oneBack, twoBack)) );  
		}
		else {
			backrel = Double.MAX_VALUE;
		}
		
		if(backrel > 1) {
			
			return 0d;
		}
		else {
			return 1.0 - backrel;
		}
		
		
	}
	
	private double norm(ArrayList<Node> nodes) {
		double result = 0d;
		
		for(Node n : nodes) {
			result += Math.pow( Math.log( (double)Waphel / (Integer)n.getProperty("backNumber") ), 2.0);
		}
		
		return result;
	}
	
	private double pageHopRank(ArrayList<Node> commonForthBack) {
		
		double result = 0d;
		
		for(Node n : commonForthBack) {
			result += 1.0 / ( (Integer)(n.getProperty("forthNumber")) );
		}
		
		return result;
		
	}
	
	private double pageRank(ArrayList<Node> backNodes) {
		
		double result = 0d;
		
		for(Node n : backNodes) {
			result += 1.0 / ((Integer)(n.getProperty("forthNumber")) );
		}
		return result;
	}

	public NodePreparator getNodeOne() {
		return nodeOne;
	}

	public NodePreparator getNodeTwo() {
		return nodeTwo;
	}
	
	
	
	
	
	

	
	
	
	
	
	
}

