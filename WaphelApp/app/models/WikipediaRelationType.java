package models;

import org.neo4j.graphdb.RelationshipType;


public enum WikipediaRelationType implements RelationshipType { LINK }
