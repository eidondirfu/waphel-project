package models;


import java.util.ArrayList;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;


public class NodePreparator {

	private Node nodeOne;
	private ArrayList<Node> backNodes;
	private ArrayList<Node> forthNodes;
	private int backSize;
	private int forthSize;

	public NodePreparator(Node nodeOne) {
		this.nodeOne = nodeOne;
		this.backNodes = getNodes(nodeOne, Direction.INCOMING);
		this.forthNodes = getNodes(nodeOne, Direction.OUTGOING);
		
		this.backSize = (Integer)(nodeOne.getProperty("backNumber"));
		this.forthSize = (Integer)(nodeOne.getProperty("forthNumber"));
	}

	public Node getNodeOne() {
		return nodeOne;
	}

	public ArrayList<Node> getBackNodes() {
		return backNodes;
	}

	public ArrayList<Node> getForthNodes() {
		return forthNodes;
	}

	public int getBackSize() {

		return backSize;
	}

	public int getForthSize() {
		return forthSize;
	}

	private ArrayList<Node> getNodes(Node node, Direction dir) {
		Iterable<Relationship> nodes = node.getRelationships(dir);
		ArrayList<Node> result = new ArrayList<Node>();

		if (dir.equals(Direction.INCOMING)) {
			for (Relationship rel : nodes) {
				result.add(rel.getStartNode());
			}
		}
		if (dir.equals(Direction.OUTGOING)) {
			for (Relationship rel : nodes) {
				result.add(rel.getEndNode());
			}
		}
		return result;
	}

}

