package models;






import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.Path;
import org.neo4j.kernel.Traversal;


import play.libs.Json;




public class Neo4jGraphDatabase {

	//protected static final String DB_PATH = "/Users/eidonfiloi/Waphel/neo4j-community-1.9.M04/data/waphelGraphSingleEdgeForth.db";
	protected static final String DB_PATH = "D:\\PROJECTS\\_waphel\\neo4j-community-1.9.M04\\data\\waphelGraphSingleEdgeForth.db";
	protected static GraphDatabaseService graphDB = new RestGraphDatabase("http://localhost:7474/db/data/");


	private static IndexManager indexManager = graphDB.index();
	private static Index<Node> index = indexManager.forNodes("pages");
	
	private static ArrayBlockingQueue<Object> queue;
	private static ArrayBlockingQueue<Object> backQueue;
	private static NodePreparator onePrep;
	private static HashMap<String,Double> rels;
	private static HashMap<String,Double> backRels;

	protected final static int WAPHEL = 4141575;
	private final static Object POISON_PILL = new Object();

	

	public static List getLinks(String pageTitle) {

		List result = new LinkedList();

		Node startNode = index.get("title", pageTitle).getSingle();

		Map mZero = new HashMap();
		mZero.put("title",startNode.getProperty("title"));
		result.add(mZero);
		
		for(Relationship rel : startNode.getRelationships(Direction.OUTGOING)) {

			Map m = new HashMap();
			m.put("title",rel.getEndNode().getProperty("title"));
			result.add(m);

		}

		//graphDB.shutdown(); ///
		return result;


	}

	public static List findPath(String startNodeTitle, String endNodeTitle) {

		List result = new LinkedList();

		PathFinder<Path> pathFinder = GraphAlgoFactory.allSimplePaths(
				Traversal.expanderForTypes( WikipediaRelationType.LINK, Direction.OUTGOING ), 5);
				
		Node startNode = index.get("title", startNodeTitle).getSingle();
		Node endNode = index.get("title", endNodeTitle).getSingle();
		
		//Iterable<Path> resultPaths = pathFinder.findAllPaths(startNode, endNode);
		Path resultPath = pathFinder.findSinglePath(startNode, endNode);
		
		if(resultPath != null) {

			for(Node n : resultPath.nodes()) {

				Map m = new HashMap();
				m.put("title", n.getProperty("title"));
				result.add(m);
			}			
			
		} else {

			result = null;
		}


		return result;
	}


	/*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList<ArrayList<ArrayList<String>>> getEdges(String pageTitle) {

		graphDB = new EmbeddedGraphDatabase(DB_PATH);
		registerShutdownHook(graphDB);

		//graphDB = new RestGraphDatabase("http://localhost:7474/db/data/");

		indexManager = graphDB.index();
		index = indexManager.forNodes("pages");

		Node startNode = index.get("title", pageTitle).getSingle();
		onePrep = new NodePreparator(startNode);
		
		ArrayList<Node> adjacentNodes = new ArrayList<Node>();
		ArrayList<Node> backAdjacentNodes = new ArrayList<Node>();

		
		for(Relationship rel : startNode.getRelationships(Direction.OUTGOING)) {

			adjacentNodes.add(rel.getEndNode());
		}

		for(Relationship rel : startNode.getRelationships(Direction.INCOMING)) {

			backAdjacentNodes.add(rel.getStartNode());
		}
		
		int size = adjacentNodes.size();
		int backSize = backAdjacentNodes.size();

		rels = new HashMap<String,Double>();
		backRels = new HashMap<String, Double>();
		
		queue = new ArrayBlockingQueue<Object>(2*size);
		backQueue = new ArrayBlockingQueue<Object>(2*backSize);

		
		for(Node n : adjacentNodes) {
			queue.add(n);
		}

		queue.add(POISON_PILL);

		
		for(Node n : backAdjacentNodes) {
			backQueue.add(n);
		}

		backQueue.add(POISON_PILL);


		ExecutorService executor = Executors.newCachedThreadPool();
		
		for(int i = 0; i < 6; i++) {
			executor.submit(new Runnable() {

				@Override
				public void run() {
					try {
						worker();
						backWorker();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			});
		}
		
		executor.shutdown();
		
		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		ArrayList<Map.Entry<String,Double>> myL = new ArrayList(rels.entrySet());
		ArrayList<Map.Entry<String,Double>> myBackL = new ArrayList(backRels.entrySet());


	
		
		Collections.sort(myL, new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				
				return -((Double)((Map.Entry)o1).getValue()).compareTo((Double)((Map.Entry)o2).getValue());
			}
			
		});

		Collections.sort(myBackL, new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				
				return -((Double)((Map.Entry)o1).getValue()).compareTo((Double)((Map.Entry)o2).getValue());
			}
			
		});

		ArrayList<ArrayList<String>> titleWeightPairs = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> titleWeightPairsBack = new ArrayList<ArrayList<String>>();


		for(Map.Entry<String,Double> entry : myL) {

		ArrayList<String> singlePairs = new ArrayList<String>();

		singlePairs.add(entry.getKey());
		singlePairs.add(entry.getValue().toString());

  		titleWeightPairs.add(singlePairs);

  	}
		for(Map.Entry<String,Double> entry : myBackL) {

		ArrayList<String> singlePairs = new ArrayList<String>();

		singlePairs.add(entry.getKey());
		singlePairs.add(entry.getValue().toString());

  		titleWeightPairsBack.add(singlePairs);

  	}

  		ArrayList<ArrayList<ArrayList<String>>> bothLists = new ArrayList<ArrayList<ArrayList<String>>>();

  		bothLists.add(titleWeightPairs);
  		bothLists.add(titleWeightPairsBack);
		
		graphDB.shutdown();
		
		return bothLists;



	}
	
	private static void worker() throws InterruptedException {
		
		while(true) {
			
			Object n = queue.take();
			if(n.equals(POISON_PILL)) {
				queue.add(POISON_PILL);
				break;
			}
			
			NodePreparator np = new NodePreparator((Node)n);
			RelevanceComputer relC = new RelevanceComputer(onePrep, np);
			double relevance = relC.getRelevance();
			rels.put((String)(((Node)n).getProperty("title")), relevance);		
		}
	}

	private static void backWorker() throws InterruptedException {
		
		while(true) {
			
			Object n = backQueue.take();
			if(n.equals(POISON_PILL)) {
				backQueue.add(POISON_PILL);
				break;
			}
			
			NodePreparator np = new NodePreparator((Node)n);
			RelevanceComputer relC = new RelevanceComputer(np, onePrep);
			double relevance = relC.getRelevance();
			backRels.put((String)(((Node)n).getProperty("title")), relevance);		
		}
	}
	
	*/

	private static void registerShutdownHook(final GraphDatabaseService graphDB) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDB.shutdown();
			}
		});
	}

}

