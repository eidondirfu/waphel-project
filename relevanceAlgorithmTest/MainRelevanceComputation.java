package relevanceAlgorithmTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.tooling.GlobalGraphOperations;

public class MainRelevanceComputation {

	protected static final String DB_PATH = "/Users/eidonfiloi/Downloads/neo4j-community-1.9.M03/data/waphelGraphSingleEdgeForth.db";
	protected static GraphDatabaseService graphDB;

	protected static Iterable<Node> nodesAll;
	protected static Iterable<Relationship> edgesAll;
	protected static ArrayList<Node> adjNodes;
	protected static ConcurrentHashMap<Long, NodePreparator> preparedNodes;
	protected static ArrayBlockingQueue<Object> edgesAllQueue;
	protected static ArrayBlockingQueue<Object> nodesAllQueue;

	protected static final Object POISON_PILL = new Object();

	// protected static Node startNode;

	protected static int numberOfNodes = 0;

	protected final static int WAPHEL = 4141575;

	protected static Object lock = new Object();
	protected static Object lockQueue = new Object();

	private static IndexManager indexManager;
	private static Index<Node> index;

	private static NodePreparator startNodePrepared;

	public static void main(String[] args) {

		Map<String, String> config = new HashMap<String, String>();
		config.put("use_memory_mapped_buffers", "true");
		config.put("neostore.nodestore.db.mapped_memory", "90M");
		config.put("neostore.relationshipstore.db.mapped_memory", "5G");
		config.put("neostore.propertystore.db.mapped_memory", "1G");
		config.put("neostore.propertystore.db.strings.mapped_memory", "500M");
		config.put("cache_type", "weak");

		graphDB = new EmbeddedGraphDatabase(DB_PATH, config);
		registerShutdownHook(graphDB);

		indexManager = graphDB.index();
		index = indexManager.forNodes("pages");

		GlobalGraphOperations global = GlobalGraphOperations.at(graphDB);

		long start = System.currentTimeMillis();

		// nodesAll = global.getAllNodes();
		edgesAll = global.getAllRelationships();

		/*
		 * Node startNode = index.get("title", "Anarchism").getSingle();
		 * Iterable<Relationship> adjEdges =
		 * startNode.getRelationships(Direction.OUTGOING); startNodePrepared =
		 * new NodePreparator(startNode);
		 */

		// edgesAllQueue = new ArrayBlockingQueue<Object>(600);
		edgesAllQueue = new ArrayBlockingQueue<Object>(200000000);
		preparedNodes = new ConcurrentHashMap<Long, NodePreparator>(5000000,
				0.75f, 100);

		// int j = 0;

		ExecutorService executor = Executors.newCachedThreadPool();

		executor.submit(new Runnable() {

			@Override
			public void run() {

				try {
					edgeAdd();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});

		for (int i = 0; i < 100; i++) {

			executor.submit(new Runnable() {

				@Override
				public void run() {

					try {
						worker();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});
		}

		executor.shutdown();

		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		long end = System.currentTimeMillis();

		System.out.println("time: " + (end - start));

		graphDB.shutdown();

	}

	private static void edgeAdd() throws InterruptedException {

		int j = 1;
		//int k = 1;
		int number = 0;
		for (Relationship rel : edgesAll) {

			if (j % 20000 == 0) {
				System.out.println(number);

				Thread.sleep(90000);

			}

			if (!rel.hasProperty("ngdRelevance")) {

				//if (k % 1000 == 0) {
					edgesAllQueue.add(rel);
					j++;

				//}
				//k++;
			} else {
				number++;
			}

		}

		edgesAllQueue.add(POISON_PILL);

	}

	private static void worker() throws InterruptedException {

		Transaction tx = graphDB.beginTx();

		int Waphel = 4141575;
		int i = 0;
		try {
			while (true) {
				Object o = edgesAllQueue.take();
				if (o.equals(POISON_PILL)) {
					edgesAllQueue.add(POISON_PILL);
					break;
				}

				Node nodeOne = ((Relationship) o).getStartNode();
				long idOne = nodeOne.getId();
				Node nodeTwo = ((Relationship) o).getEndNode();
				long idTwo = nodeTwo.getId();

				NodePreparator onePrep = preparedNodes.get(idOne);
				NodePreparator twoPrep = preparedNodes.get(idTwo);

				if (onePrep == null) {
					onePrep = new NodePreparator(nodeOne);
					preparedNodes.putIfAbsent(idOne, onePrep);

				}

				if (twoPrep == null) {
					twoPrep = new NodePreparator(nodeTwo);
					preparedNodes.putIfAbsent(idTwo, twoPrep);

				}

				if (!((Relationship) o).hasProperty("ngdRelevance")) {

					RelevanceComputer relComp = new RelevanceComputer(onePrep,
							twoPrep);
					double relevance = relComp.getRelevance();

					((Relationship) o).setProperty("ngdRelevance", relevance);
					synchronized (lock) {
						numberOfNodes++;
					}

					if (numberOfNodes % 100 == 0) {
						System.out.println(numberOfNodes + " "
								+ nodeOne.getProperty("title") + " ---> "
								+ nodeTwo.getProperty("title") + " "
								+ relevance);
					}
				} else {
					System.out.println(numberOfNodes + " already done");

				}

				if (i % 200 == 0) {
					tx.success();
					tx.finish();
					tx = graphDB.beginTx();
					// System.out.println(Thread.currentThread().getName()
					// + ": flush");
				}
			}
		} finally {
			tx.finish();
		}
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDB) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDB.shutdown();
			}
		});
	}

}
