package relevanceAlgorithmTest;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.kernel.EmbeddedGraphDatabase;

public class Test {

	protected static final String DB_PATH = "/Users/eidonfiloi/Downloads/neo4j-community-1.9.M04/data/waphelGraphSingleEdgeForth.db";
	protected static GraphDatabaseService graphDB;

	protected static Iterable<Node> nodesAll;
	protected static Iterable<Relationship> edgesAll;
	protected static ArrayList<Node> adjNodes;
	protected static ConcurrentHashMap<Node, NodePreparator> preparedNodes;
	protected static ArrayBlockingQueue<Object> edgesAllQueue;
	protected static ArrayBlockingQueue<Object> nodesAllQueue;

	protected static final Object POISON_PILL = new Object();

	// protected static Node startNode;

	protected static int numberOfNodes = 0;

	protected final static int WAPHEL = 4141575;

	protected static Object lock = new Object();
	private static IndexManager indexManager;
	private static Index<Node> index;
	
	
	
	private static HashMap<Node,Double> rels;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {

		Map<String, String> config = new HashMap<String, String>();
		config.put("use_memory_mapped_buffers", "true");
		config.put("neostore.nodestore.db.mapped_memory", "90M");
		config.put("neostore.relationshipstore.db.mapped_memory", "5G");
		config.put("neostore.propertystore.db.mapped_memory", "1G");
		config.put("neostore.propertystore.db.strings.mapped_memory", "500M");
		config.put("cache_type", "weak");

		graphDB = new EmbeddedGraphDatabase(DB_PATH, config);
		registerShutdownHook(graphDB);

		indexManager = graphDB.index();
		index = indexManager.forNodes("pages");
		rels = new HashMap<Node,Double>();
		
		long start = System.currentTimeMillis();
				

		Node startNode = index.get("title", "Napoleon").getSingle();
		NodePreparator onePrep = new NodePreparator(startNode);

		//Node endNode = index.get("title", "Simple living").getSingle();
		
		int i = 0;
		for(Relationship rel : startNode.getRelationships(Direction.OUTGOING)) {
			
			RelevanceComputer relC = new RelevanceComputer(onePrep, new NodePreparator(rel.getEndNode()));
			double relevance = relC.getRelevance();
			rels.put(rel.getEndNode(), relevance);
			
			i++;
			System.out.println(i);// + " " + rel.getEndNode().getProperty("title") + " " + relevance);
		}
		
		List myL = new ArrayList(rels.entrySet());
		
		
		
		Collections.sort(myL, new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				
				return -((Double)((Map.Entry)o1).getValue()).compareTo((Double)((Map.Entry)o2).getValue());
			}
			
		});
		
		int k = 0;
		for(Iterator it = myL.iterator(); it.hasNext(); ) {
			k++;
			Map.Entry me = (Map.Entry)it.next();
			System.out.println(k + " " +  ((Node)(me).getKey()).getProperty("title") + " " +  ((Double)(me).getValue()));
		}
		
		
		
		
				//NodePreparator twoPrep = new NodePreparator(endNode);
		
				
		//RelevanceComputer relComp = new RelevanceComputer(onePrep, twoPrep);
		//double relevance = relComp.getRelevance(WAPHEL);
		
				
		//System.out.println(numberOfNodes + " " + startNode.getProperty("title") + " ---> " + endNode.getProperty("title") + " " + relevance);

		


		
		
		

			
			
		
		long end = System.currentTimeMillis();

		System.out.println("time: " + (end - start));

		graphDB.shutdown();

	}
	
	

	private static void registerShutdownHook(final GraphDatabaseService graphDB) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDB.shutdown();
			}
		});
	}

}

