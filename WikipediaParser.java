import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


public class WikipediaParser {

	protected static final String XML_DUMP_PATH = "D:\\PETY\\WIKIPEDIA GRAPH PROJECT\\enwiki-20130102-pages-articles.xml";
	protected static final String XML_OUT_PATH = "D:\\PETY\\WIKIPEDIA GRAPH PROJECT\\waphel-20130102-pages-and-links.xml";
	protected static XMLOutputFactory factory;
	protected static XMLStreamWriter writer;
	protected static InputStreamReader reader;

	public static void main(String[] args) {

		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			factory = XMLOutputFactory.newInstance();

			writer = factory.createXMLStreamWriter(new FileOutputStream(
					XML_OUT_PATH, true), "UTF-8");

			writer.writeStartDocument();
			writer.writeStartElement("document");

			File file = new File(XML_DUMP_PATH);
			InputStream inputStream = new FileInputStream(file);
			reader = new InputStreamReader(inputStream, "UTF-8");
			InputSource inputSource = new InputSource(reader);
			inputSource.setEncoding("UTF-8");

			xmlReader.setContentHandler(new WikipediaContentHandler());

			long start = System.currentTimeMillis();
			System.out.println("parse started...");
			xmlReader.parse(inputSource);
			writer.writeEndElement();
			writer.writeEndDocument();
			writer.close();
			reader.close();
			long end = System.currentTimeMillis();
			System.out.println("total time: " + (end - start));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} finally {
			if (reader != null || writer != null) {
				try {
					reader.close();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
