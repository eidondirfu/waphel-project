define([
	'jquery',
	'underscore',
	'backbone'
], function($,_,Backbone){
	
	var WLink = function(){
		var my = {}, that = {};

		that.source = undefined;
		that.target = undefined;
		that.id = "fakeId";
		that.belongsToSearchPath = false;

		that.create = function(source,target) {
			that.source = source;
			that.target = target;
			that.id = source.id + target.id;
			if (target.belongsToSearchPath) {
				that.belongsToSearchPath = true;
			}
		};

		return that;
	};

	return WLink;
});