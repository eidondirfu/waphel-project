define([
	'jquery',
	'underscore',
	'backbone'
], function($,_,Backbone){
	
	var WNode = function(){

		var my = {}, that = {};

		that.id = "fakeId";
		that.weight = 2;
		that.relevantNodeList = [];
		that.x = 0;
		that.y = 0;
		that.px = 0;
		that.py = 0;
		that.belongsToSearchPath = false;
		that.depth = 0;
		that.parentNode = undefined;
		that.title = "fakeTitle"

		that.create = function(t,id,depth,pn) {
			that.title = t;
			that.id = id;
			if (depth) {
				that.depth = depth;
			}
			if (pn) {
				that.parentNode = pn;
			}
		};

		that.setParentNode = function(pn) {
			that.parentNode = pn;
		};

		that.addRelevantNodes = function(relNodes) {
			if(Object.prototype.toString.call( relNodes ) === '[object Array]') {
				that.relevantNodeList = that.relevantNodeList.concat(relNodes);
			} else {
				that.relevantNodeList.push(relNodes);	
			}
		};

		return that;
	};

	return WNode;
});