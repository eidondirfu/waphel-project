define([
	'jquery',
	'underscore',
	'backbone',
	'models/WNode',
	'models/WLink'
], function($,_,Backbone,WNode,WLink){
	

	var WGraph = Backbone.Model.extend({

		nodeData : [{"title":"Albert Einstein","relevance":1.0},{"title":"General relativity","relevance":0.7673097996203391},{"title":"Special relativity","relevance":0.7416374488320472},{"title":"Quantum mechanics","relevance":0.7318511951603848},{"title":"Max Planck","relevance":0.7156239538887339},{"title":"Spacetime","relevance":0.6803537963634225},{"title":"Niels Bohr","relevance":0.6761834378370509},{"title":"Speed of light","relevance":0.6713024354804012},{"title":"Erwin Schr?dinger","relevance":0.6686179984521582},{"title":"Classical mechanics","relevance":0.6604116357026529},{"title":"Max Born","relevance":0.6556160634944457},{"title":"Gravitation","relevance":0.6549817311486165},{"title":"Luminiferous aether","relevance":0.6457115912109889},{"title":"Arthur Eddington","relevance":0.6440923204064511},{"title":"Nobel Prize in Physics","relevance":0.6412411941390449},{"title":"Theoretical physics","relevance":0.640801821167381},{"title":"Henri Poincar?","relevance":0.6404084447263465},{"title":"Mass\u2013energy equivalence","relevance":0.6391265011501135},{"title":"Photon","relevance":0.6378380319402864},{"title":"Maxwell's equations","relevance":0.6360883815089698},{"title":"Einstein field equations","relevance":0.63292612668865},{"title":"Ernest Rutherford","relevance":0.6326961831196336},{"title":"Photoelectric effect","relevance":0.6324188749799379},{"title":"J. Robert Oppenheimer","relevance":0.6274097514306677},{"title":"Arnold Sommerfeld","relevance":0.6241063607807499},{"title":"Hermann Minkowski","relevance":0.6239369319268605},{"title":"SPACE","relevance":0.6233278830399188},{"title":"Louis de Broglie","relevance":0.622225557048835},{"title":"Physicist","relevance":0.6219464218449862},{"title":"Principle of relativity","relevance":0.6212022568747667},{"title":"Ernst Mach","relevance":0.6196453551861171},{"title":"Wave\u2013particle duality","relevance":0.6191497366937057},{"title":"Le? Szil?rd","relevance":0.6174639993485068},{"title":"Eugene Wigner","relevance":0.6151347094138545},{"title":"Annus Mirabilis papers","relevance":0.61499694866394},{"title":"Length contraction","relevance":0.6131016352579226},{"title":"Bertrand Russell","relevance":0.6125829664091728},{"title":"Cosmological constant","relevance":0.6125271162571572},{"title":"Equivalence principle","relevance":0.6097197764957635},{"title":"EPR paradox","relevance":0.6092018172050485},{"title":"Time dilation","relevance":0.606909474699812},{"title":"Atom","relevance":0.6050180542126119},{"title":"Matter","relevance":0.6048086560139041},{"title":"Planck constant","relevance":0.604055284992988},{"title":"Mileva Mari?","relevance":0.6027200651053454},{"title":"Sigmund Freud","relevance":0.6009322656748726},{"title":"Black hole","relevance":0.6005810663178467},{"title":"Quantum","relevance":0.5992567599970428},{"title":"Einstein\u2013Szil?rd letter","relevance":0.598456944855857},{"title":"Nathan Rosen","relevance":0.5984360157386984},{"title":"John von Neumann","relevance":0.5976871334435871},{"title":"Wilhelm Wien","relevance":0.5971334988922182},{"title":"Edward Teller","relevance":0.5959656040009963},{"title":"Brownian motion","relevance":0.5955358156281002},{"title":"Annalen der Physik","relevance":0.5930674616525875},{"title":"Gravitational lens","relevance":0.5929762197422205},{"title":"Momentum","relevance":0.592295550458783},{"title":"Universe","relevance":0.5920081403980701},{"title":"Gravitational wave","relevance":0.5919420267170972},{"title":"Physics","relevance":0.5905643934952939},{"title":"Bell test experiments","relevance":0.59029476845434},{"title":"Black body","relevance":0.5899983485312874},{"title":"Bell's theorem","relevance":0.5890350716660808},{"title":"Mach's principle","relevance":0.588894651342188},{"title":"Robert Andrews Millikan","relevance":0.5864037294377057},{"title":"Unified field theory","relevance":0.5821798096917505},{"title":"Bose\u2013Einstein condensate","relevance":0.5813080220984027},{"title":"String theory","relevance":0.5803872624634707},{"title":"Manhattan Project","relevance":0.5799183541815206},{"title":"Satyendra Nath Bose","relevance":0.5782139062529843},{"title":"Peter Debye","relevance":0.5773512110025858},{"title":"European Physical Journal","relevance":0.5767093310224635},{"title":"Schwarzschild metric","relevance":0.5764891479774888},{"title":"Weak interaction","relevance":0.5752312340382523},{"title":"Thought experiment","relevance":0.574470459444176},{"title":"Strong interaction","relevance":0.5743521783789494},{"title":"Kurt G?del","relevance":0.5740670539259602},{"title":"Gravitational field","relevance":0.5740461907158566},{"title":"Hans Albert Einstein","relevance":0.5728533537698961},{"title":"Otto Stern","relevance":0.5723473380204885},{"title":"Thermodynamics","relevance":0.5689636021677422},{"title":"Gravitational time dilation","relevance":0.5685756927189368},{"title":"Lev Landau","relevance":0.5659850257841482},{"title":"Institute for Advanced Study","relevance":0.565433680379215},{"title":"Theory of everything","relevance":0.5644604077296643},{"title":"Statistical mechanics","relevance":0.5634499532064917},{"title":"John Earman","relevance":0.5633359764777415},{"title":"Elsa Einstein","relevance":0.5629663016488917},{"title":"Eduard Einstein","relevance":0.5627641494018665},{"title":"Static universe","relevance":0.5624629559242158},{"title":"Modern physics","relevance":0.5603752297170554},{"title":"General covariance","relevance":0.559690985044567},{"title":"Force","relevance":0.5590339318616364},{"title":"Relativity priority dispute","relevance":0.5587700964196388},{"title":"Einstein family","relevance":0.557887146897087},{"title":"Russell\u2013Einstein Manifesto","relevance":0.5576293677317722},{"title":"Deutsche Physikalische Gesellschaft","relevance":0.5565969901767702},{"title":"Compton scattering","relevance":0.5549602684446618},{"title":"Frame-dragging","relevance":0.554885443658136},{"title":"Chaim Weizmann","relevance":0.554255114125708},{"title":"Philosophy of science","relevance":0.5538230452878098},{"title":"Linus Pauling","relevance":0.5532978072512004},{"title":"Nuclear fission","relevance":0.5528897966071367},{"title":"Astrophysics","relevance":0.5523784747922935},{"title":"Boson","relevance":0.5522709447421592},{"title":"Electromagnetic field","relevance":0.5519291927995509},{"title":"Leopold Infeld","relevance":0.5509788900110444},{"title":"Solar eclipse of May 29, 1919","relevance":0.5508696069977229},{"title":"Gauge theory","relevance":0.5504650294390071},{"title":"Introduction to quantum mechanics","relevance":0.550062842962511},{"title":"Aether theories","relevance":0.5497492055340674},{"title":"Principle of locality","relevance":0.5494587292387968},{"title":"Gilbert N. Lewis","relevance":0.5493015458831002},{"title":"Lagrangian","relevance":0.5478930011067991},{"title":"Bose\u2013Einstein statistics","relevance":0.5476458144952199},{"title":"Abraham Pais","relevance":0.5427021845161683},{"title":"Peter Bergmann","relevance":0.53949111038261},{"title":"Classical unified field theories","relevance":0.5392586326265529},{"title":"Evgeny Lifshitz","relevance":0.538208126579104},{"title":"Max Planck Medal","relevance":0.5380465165255626},{"title":"Eric Allin Cornell","relevance":0.5372366725049955},{"title":"Invariant mass","relevance":0.536440212235772},{"title":"Gravitoelectromagnetism","relevance":0.535837835575518},{"title":"Maser","relevance":0.5344616144374873},{"title":"Quantization (physics)","relevance":0.5341017275321384},{"title":"Carl Wieman","relevance":0.5338677649972722},{"title":"John Stachel","relevance":0.5336356102545059},{"title":"Einstein refrigerator","relevance":0.5303619249762124},{"title":"Matter wave","relevance":0.5301132764386394},{"title":"Geodesic","relevance":0.529811298396249},{"title":"Sticky bead argument","relevance":0.5286661717236559},{"title":"Wormhole","relevance":0.5278484730797595},{"title":"History of physics","relevance":0.5275509215110512},{"title":"Superconductivity","relevance":0.5261454118890131},{"title":"The Evolution of Physics","relevance":0.5259499307692072},{"title":"Humboldt University of Berlin","relevance":0.5248544542752569},{"title":"Annus mirabilis","relevance":0.5228458512432045},{"title":"Scientific American","relevance":0.5219585450777722},{"title":"Periodic table","relevance":0.5199058725982637},{"title":"Matteucci Medal","relevance":0.5182928966151924},{"title":"Hole argument","relevance":0.5182524428503706},{"title":"Introduction to special relativity","relevance":0.5179958600865163},{"title":"Prussian Academy of Sciences","relevance":0.5174858748137933},{"title":"Princeton, New Jersey","relevance":0.5173505164563569},{"title":"Physics Today","relevance":0.5169745103621873},{"title":"Point particle","relevance":0.5165394733234725},{"title":"Hamilton\u2013Jacobi equation","relevance":0.5164669680048625},{"title":"Laser","relevance":0.5108671190636019},{"title":"Stimulated emission","relevance":0.5099111648273316},{"title":"Olympia Academy","relevance":0.5097133478428754},{"title":"Critique of Pure Reason","relevance":0.5083984922344729},{"title":"German nuclear energy project","relevance":0.5075624417017525},{"title":"Kaiser Wilhelm Society","relevance":0.5063618142864172},{"title":"Alfred Kleiner","relevance":0.5058470087375404},{"title":"ETH Zurich","relevance":0.5048569585731956},{"title":"David Hume","relevance":0.5041696682623443},{"title":"Jila","relevance":0.5034212263250545},{"title":"Euclid's Elements","relevance":0.5023607670533521},{"title":"Swiss Federal Institute of Intellectual Property","relevance":0.5018629118615695},{"title":"Chain reaction","relevance":0.5004520391188559},{"title":"Historical Museum of Bern","relevance":0.4983859713832056},{"title":"Adiabatic invariant","relevance":0.4975535932948257},{"title":"California Institute of Technology","relevance":0.4972146417860603},{"title":"Adolf Hitler","relevance":0.49520711365989967},{"title":"Noether's theorem","relevance":0.49519542650690096},{"title":"Genius","relevance":0.4945893327804294},{"title":"Torsion tensor","relevance":0.4937646876407618},{"title":"List of German inventors and discoverers","relevance":0.4918964557810767},{"title":"Thomas Stoltz Harvey","relevance":0.4912861524759522},{"title":"Nobel Foundation","relevance":0.49016125675744104},{"title":"Heinrich Friedrich Weber","relevance":0.4891880585236376},{"title":"Spinozism","relevance":0.48570556452286867},{"title":"Privatdozent","relevance":0.47810746652370395},{"title":"Free fall","relevance":0.4780830298730225},{"title":"List of Nobel laureates in Physics","relevance":0.47759910355142066},{"title":"Menachem Ussishkin","relevance":0.47664957353941984},{"title":"Roy Kerr","relevance":0.4757341037043962},{"title":"Luitpold Gymnasium","relevance":0.47418302291499387},{"title":"Nazi book burnings","relevance":0.4731425393987209},{"title":"Princeton University Press","relevance":0.4724954641200084},{"title":"Torsion spring","relevance":0.47197891854434365},{"title":"Einstein's unsuccessful investigations","relevance":0.47128691239100756},{"title":"Einstein notation","relevance":0.469707055359065},{"title":"Hebrew University of Jerusalem","relevance":0.46936834085396},{"title":"Ideal gas","relevance":0.46737356857849977},{"title":"List of scientific publications by Albert Einstein","relevance":0.46723690357544945},{"title":"Office of Scientific and Technical Information","relevance":0.46565968070745556},{"title":"Franklin D. Roosevelt","relevance":0.4650834220756817},{"title":"Solar eclipse","relevance":0.4648132717616342},{"title":"Action-angle coordinates","relevance":0.4645195475613119},{"title":"Bose\u2013Einstein correlations","relevance":0.4629593236813164},{"title":"List of Jewish Nobel laureates","relevance":0.4622566283478966},{"title":"W. E. B. Du Bois","relevance":0.4621324192151392},{"title":"David Ben-Gurion","relevance":0.4608702520222774},{"title":"Wien's displacement law","relevance":0.4598506001681108},{"title":"Rayleigh scattering","relevance":0.45949299082922634},{"title":"Royal Society","relevance":0.4579881931804667},{"title":"Baer's law","relevance":0.4565977743965328},{"title":"Charles University in Prague","relevance":0.4562725543290547},{"title":"Joseph Goebbels","relevance":0.45552835466352387},{"title":"Particle","relevance":0.45476694890121117},{"title":"Vera Weizmann","relevance":0.4497319822487493},{"title":"University of Zurich","relevance":0.44896797631527574},{"title":"Ernst G. Straus","relevance":0.448075786197019},{"title":"Absorption refrigerator","relevance":0.4475100443070559},{"title":"Bruria Kaufman","relevance":0.44722942827772777},{"title":"The Einstein Theory of Relativity","relevance":0.4428917719023039},{"title":"Second quantization","relevance":0.4404993606404825},{"title":"Personal god","relevance":0.4404559373298994},{"title":"List of coupled cousins","relevance":0.4284573366724131},{"title":"Copley Medal","relevance":0.42677852277450584},{"title":"Absent-minded professor","relevance":0.42026166786262925},{"title":"Heinrich Burkhardt","relevance":0.4200748926142875},{"title":"National Institute of Standards and Technology","relevance":0.41941981909014886},{"title":"Wolfgang Amadeus Mozart","relevance":0.4190021659317502},{"title":"Ultracold atom","relevance":0.41812394147518883},{"title":"Mad scientist","relevance":0.4154152612240032},{"title":"Ludwig van Beethoven","relevance":0.4153374493189061},{"title":"Judaism","relevance":0.40925126731148653},{"title":"Leiden University","relevance":0.4077099511644432},{"title":"Conscription in Germany","relevance":0.4063137628575478},{"title":"President of Israel","relevance":0.4040883325497557},{"title":"Monthly Review","relevance":0.3991962110981076},{"title":"Eth","relevance":0.39676961419379697},{"title":"Patent office","relevance":0.3961991545284178},{"title":"Bern","relevance":0.3914600329688754},{"title":"Capillary action","relevance":0.39081424994221364},{"title":"Translational symmetry","relevance":0.38991673291143736},{"title":"Electrical engineering","relevance":0.38928630229671884},{"title":"Albert Einstein's brain","relevance":0.3858208770400756},{"title":"Raziuddin Siddiqui","relevance":0.3854534997090869},{"title":"War of Currents","relevance":0.37999179798731414},{"title":"Life (magazine)","relevance":0.378638625967608},{"title":"Statistical physics","relevance":0.37851210065155544},{"title":"Personality rights","relevance":0.3784144942037101},{"title":"Socialism","relevance":0.3745979740672248},{"title":"Open Court Publishing Company","relevance":0.37434276386362236},{"title":"Historical Studies in the Natural Sciences","relevance":0.37411233821954654},{"title":"Corbis","relevance":0.3736840160015276},{"title":"Zurich","relevance":0.37292905056328424},{"title":"Herbert Samuel, 1st Viscount Samuel","relevance":0.37257133253021574},{"title":"Austria-Hungary","relevance":0.3722050901107451},{"title":"German Empire","relevance":0.37136079989867954},{"title":"Abba Eban","relevance":0.369182258076419},{"title":"African-American Civil Rights Movement (1896\u20131954)","relevance":0.3684044497242607},{"title":"University Medical Center of Princeton at Plainsboro","relevance":0.36693967405571315},{"title":"Neuroscience","relevance":0.36599893518148185},{"title":"Nazi Germany","relevance":0.3654792973852199},{"title":"Alternating current","relevance":0.3642034892075023},{"title":"Quantum chaos","relevance":0.36390016833535194},{"title":"Matura","relevance":0.36315295146582094},{"title":"Marxists Internet Archive","relevance":0.3613266634446978},{"title":"Nissen fundoplication","relevance":0.3464542511333667},{"title":"Citizenship in the United States","relevance":0.34319679974894957},{"title":"Time (magazine)","relevance":0.3398481425030369},{"title":"Patent examiner","relevance":0.3394309067316913},{"title":"National Association for the Advancement of Colored People","relevance":0.33270567660846734},{"title":"British Mandate for Palestine (legal instrument)","relevance":0.32855453830936865},{"title":"King's College London","relevance":0.32322172844904473},{"title":"Electrolux","relevance":0.3194511199865847},{"title":"Rote learning","relevance":0.3164787747490829},{"title":"University of Bern","relevance":0.31470527646812446},{"title":"History News Network","relevance":0.3126202161470657},{"title":"Ulm","relevance":0.31077581997547443},{"title":"Statelessness","relevance":0.3077453746164895},{"title":"Cousin marriage","relevance":0.30494230819628404},{"title":"Munich","relevance":0.2975876536460903},{"title":"University of Colorado at Boulder","relevance":0.2937758189694295},{"title":"PBS","relevance":0.28946762252385194},{"title":"Patent application","relevance":0.2827431009996711},{"title":"George Mason University","relevance":0.27589428585146714},{"title":"Novi Sad","relevance":0.2751474561357948},{"title":"Leon Botstein","relevance":0.2728036045425191},{"title":"German nationality law","relevance":0.2661706516039491},{"title":"Aarau","relevance":0.2631927410192162},{"title":"Meritocracy","relevance":0.2619283617802968},{"title":"Direct current","relevance":0.24866015333201813},{"title":"Docent","relevance":0.23038456154872214},{"title":"BBC News","relevance":0.22993553144039766},{"title":"Richard Haldane, 1st Viscount Haldane","relevance":0.22199960194153734},{"title":"Pavia","relevance":0.21930876350271847},{"title":"NPR","relevance":0.21246359729118222},{"title":"BBC","relevance":0.20770210260640917},{"title":"Tokyo Imperial Palace","relevance":0.20554007372987287},{"title":"Switzerland","relevance":0.2039126047819355},{"title":"Kingdom of W?rttemberg","relevance":0.20056533026775605},{"title":"The Times","relevance":0.19849509044736421},{"title":"New York City","relevance":0.19766434401196822},{"title":"Catholic school","relevance":0.17135518724576942},{"title":"Baden-W?rttemberg","relevance":0.17018544319818893},{"title":"Singapore","relevance":0.14631901781963746},{"title":"Antwerp","relevance":0.13346546903582512},{"title":"USA","relevance":0.13040139490061803},{"title":"MILAN","relevance":0.1299864367458396},{"title":"Sri Lanka","relevance":0.11873464502611783},{"title":"Poland","relevance":0.11425922956594076},{"title":"United States","relevance":0.09890595375293472},{"title":"Chamber music","relevance":0.06853546042210568},{"title":"The Roger Richman Agency","relevance":0.06609984292718776},{"title":"Max Talmey","relevance":0.056037712287712295},{"title":"History of gravitational theory","relevance":0.008302334790653367},{"title":"Abdominal aortic aneurysm","relevance":0.006410989798423805},{"title":"Zoellner Quartet","relevance":0.005673790716966839},{"title":"Meander","relevance":0.002937816093329132},{"title":"Juilliard String Quartet","relevance":0.0017085868472191993},{"title":"Clark Glymour","relevance":0.0016895961942317934},{"title":"Olsberg, Switzerland","relevance":0.0016421096508421487},{"title":"Violin sonata","relevance":0.0016282081014223873},{"title":"John Francis Hylan","relevance":0.0016233766233766235}],
		counter : 1,
		model : {},
		nodes : [],
		links : [],
		relevantNodeCount : 7,
		startNode : {},
		currentNode : {},


		defaults: {
			
		},

		initialize: function() {
			this.set({"currentNode" : this.get("startNode")});
			this.nodes.push(this.get("startNode"));
			this.addFakeNodesTo(this.get("startNode"),this.relevantNodeCount,true,this);
		},

		setCurrentNode: function(nextID) {
			var that = this;
			var currentID = that.get("currentNode").id;

			console.log("nodes 0", that.nodes);
			console.log("current ID", currentID, " nextID", nextID);
			if (nextID === currentID) {
				console.log("Node already current");
			} else {
				var nextNode  = _.where(that.nodes,{"id" : nextID})[0];
				console.log("next node", nextNode);
				if (nextNode) {
					if (!nextNode.belongsToSearchPath) {
						nextNode.belongsToSearchPath = true;
						if (nextNode.relevantNodeList.length < 1) {
							that.addFakeNodesTo(nextNode,that.relevantNodeCount,false,that);
						}
						/*
						_.each(_.where(that.links,{"target" : nextNode}),function (li) {
							li.belongsToSearchPath = true;
							//console.log("new link", li);
						});
						*/						
					}

					var newLinks = [];
					for (var it = 0; it < that.links.length; it++) {
						var li = that.links[it];
						if (li.target.belongsToSearchPath) {
							newLinks.push(li);
						}
					}
					that.links = newLinks;

					var newNodes = [];
					for (var it = 0; it < that.nodes.length; it++) {
						var no = that.nodes[it];
						if (no.belongsToSearchPath) {
							newNodes.push(no);
						}
					}
					that.nodes = newNodes;

					for (var it = 0; it < nextNode.relevantNodeList.length; it++) {
						var node = nextNode.relevantNodeList[it];
						if ($.inArray(node,that.nodes) === -1) {
							that.nodes.push(node);
							var link = new WLink();
							link.create(nextNode,node);
							that.links.push(link);
						}
					}

					//that.links = _.where(that.links,{"belongsToSearchPath" : true});
					//that.nodes = _.where(that.nodes,{"belongsToSearchPath" : true});
					


					that.set({"currentNode" : nextNode});					
				}
			}
			console.log("nodes 1", that.nodes);
		},

		/*
		setCurrentNode: function(newID) {

			var that = this;
			console.log("newID", newID);
			var currentID = that.get("currentNode").id;
			console.log("currentID", currentID);


			if (newID == currentID) {
				console.log("Node already current");
			} else {
				//console.log("this.links", this.links);
				//console.log("that.links", that.links);

				var nextNode = _.where(that.nodes,{"id" : newID})[0];
				if (nextNode) {
					console.log("next node", nextNode);
					if (!nextNode.belongsToSearchPath) {
						nextNode.belongsToSearchPath = true;
						_.each(_.where(that.links,{"target" : nextNode}),function (li) {
							li.belongsToSearchPath = true;
						});
					}

					that.nodes = _.where(that.nodes,{"belongsToSearchPath" : true});
					that.links = _.where(that.links,{"belongsToSearchPath" : true});

					if (nextNode.relevantNodeList.length < 1) {
						that.addFakeNodesTo(nextNode,that.relevantNodeCount,false);
					}

					that.nodes = _.union(that.nodes, nextNode.relevantNodeList);
					var newLinks = _.each(nextNode.relevantNodeList,function(nextNode, node) {
						var newLink = new WLink();
						newLink.create(nextNode, node);
						this.links.push(newLink);
					}, that);
					that.links = _.union(that.links, newLinks);
					that.set({"currentNode" : nextNode});
				}
			}
		},
		*/

		/*
		setCurrentNode: function(newID) {
			if (newID === this.get("currentNode").id) {
				console.log("Node already current");
			} else {
				var found = false;
				for (var it = 0; it < this.nodes.length; it++) {
					if (newID === this.nodes[it].id) {
						var nextNode = this.nodes[it];
						if (!nextNode.belongsToSearchPath) {
							nextNode.belongsToSearchPath = true;
							if (nextNode.relevantNodeList.length < 1) {
								this.addFakeNodesTo(nextNode,this.relevantNodeCount,false);
							}	
						}
						//this.updateGraphStructure(this);
						///
						console.log("this 0", this);
						//console.log("links 0", this.links);
						var newLinkList = [];
						for (var it = 0; it < this.links.length; it++) {
							var li = this.links[it];
							if (li.target.belongsToSearchPath) {
								newLinkList.push(li);
							} else {
								console.log("deleting li", li);
							}
						}
						this.links = [];
						this.links = this.links.concat(newLinkList);
						//console.log("links 1", this.links);

						//console.log("nodes 0", this.nodes);
						var newNodeList = [];
						for (var it = 0; it < this.nodes.length; it++) {
							var no = this.nodes[it];
							if (no.belongsToSearchPath) {
								newNodeList.push(no);
							} else {
								console.log("deleting no", no);
							}
						}
						this.nodes = [];
						this.nodes = this.nodes.concat(newNodeList);
						//console.log("nodes 1", this.nodes);
						console.log("this 1", this);
						///
						this.addNodesAndLinks(nextNode);
						console.log("this 2", this);
						this.set({"currentNode" : nextNode});
						//console.log("setting current node.");
						found = true;
						break;
					}
				}
			};
			
			console.log("this.nodes", this.nodes);
			console.log("currentNode", this.get("currentNode"));
			//this.trigger("change");
		},
		*/
		/*		
		updateGraphStructure: function(that) {
			-->
			console.log("this.nodes 0", this.nodes);
			var nodesToBeDeleted = _.where(this.nodes,{"belongsToSearchPath" : false});
			console.log("nodesToBeDeleted", nodesToBeDeleted);
			_.remove(this.nodes,nodesToBeDeleted);
			console.log("this.nodes 1", this.nodes);
			//_.remove(this.links,_.where(this.links,{"target" : {"belongsToSearchPath" : false }}));
			-->

			console.log("links 0", that.links);
			var newLinkList = [];
			for (var it = 0; it < that.links.length; it++) {
				var l = that.links[it];
				if (l.target.belongsToSearchPath) {
					newLinkList.push(l);
				}
			}
			that.links = newLinkList;
			console.log("links 1", that.links);

			console.log("nodes 0", that.nodes);
			var newNodeList = [];
			for (var it = 0; it < this.nodes.length; it++) {
				var n = this.nodes[it];
				if (n.belongsToSearchPath) {
					newNodeList.push(n);
				}
			}
			this.nodes = newNodeList;
			console.log("nodes 1", that.nodes);

		},

		*/
		addNodesAndLinks: function(theNode,that) {
			for (var it = 0; it < theNode.relevantNodeList.length; it++) {
				var newNode = theNode.relevantNodeList[it];
				that.addSingleNodeAndLink(theNode,newNode,that);
			}
		},

		addSingleNodeAndLink: function(startNode, endNode, that) {
			that.nodes.push(endNode);
			var link = new WLink();
			link.create(startNode, endNode);
			that.links.push(link);			
		},

		addFakeNodesTo: function(theNode,count,addToGraph,that) {

			var depth = theNode.depth + 1;
			var id = theNode.id;
			for (var it = 0; it < count; it++) {
				var newNode = new WNode();
				newNode.create(this.nodeData[this.counter].title, "id" + that.counter, depth, theNode);
				theNode.relevantNodeList.push(newNode);
				//this.counter = (this.counter+1)%this.nodeData.length;
				that.counter++;
				if(addToGraph) {
					that.addSingleNodeAndLink(theNode,newNode,that);
				}
			}
		}

	});

	return WGraph;
});