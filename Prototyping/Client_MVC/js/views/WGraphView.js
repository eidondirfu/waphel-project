define([
	'jquery',
	'underscore',
	'backbone',
	'../models/WGraph',
	'd3'
], function($,_,Backbone,WGraph){

	var WGraphView = Backbone.View.extend({

		el : $('#container'),

		events: {"click svg g circle" : "nodeClick"},

		width : 960,

		height : 560,

		svg: {},

		initialize: function() {
			console.log(this);
			_.bindAll(this);
			this.svg = d3.select(this.el).append('svg')
				.attr("viewBox","0 0 " + this.width + " " + this.height); 
			
			this.listenTo(this.model, "change:currentNode", this.update);
			
			/*
			this.model.on('change', function() {
				console.log("change registered.");
				this.update();
			}, this);
			*/
			//this.on('change:currentNode',this.update);

			console.log("nodes",this.model.nodes);
			console.log("links",this.model.links);
			
			///////////// d3 force
			
			this.force = d3.layout.force()
				.size([this.width, this.height])
				.nodes(this.model.nodes)
				.links(this.model.links)
				.linkDistance(120)
				.charge(-120)
				.on("tick", this.tick);

			this.nodes = this.force.nodes();
			this.links = this.force.links();
			this.node = this.svg.selectAll(".node");
			this.link = this.svg.selectAll(".link");
			////////////

			this.restart();
		},

		restart : function() {
		  //this.link = this.link.data(this.links);
		  
		  //this.links[5].target.belongsToSearchPath = true;

		  //my.link.enter().insert("line", ".node")
		  console.log("view nodes", this.model.nodes);



		  this.nodes = this.svg.selectAll(".node");
  		  this.node = this.nodes.data(this.model.nodes);

  		  this.node.select("circle")
  		  	.attr("id",function(d) {
  				return d.id;
  		  	})
  		  	.attr("fill", function(d) {
  		  		if (d.belongsToSearchPath) {
  		  			return "black";
  		  		} else {
  		  			return "grey";
  		  		}
  		  	});

  		  this.node.select("text").text(function(d) {
  		  	console.log("updating", this);
  		  	return d.title; 
  		  });

		  var n = this.node.enter().append("g")
		    	.attr("class", "node")
		  		.call(this.force.drag);

			n.append("text")
	        	.text(function(d) {return d.title; })
	        	.attr('x', 0)
	        	.attr('y', -10)
	        	.attr("text-anchor", "middle");

			n.append("circle")
				.attr("id",function(d){
					//return d.title;
					return d.id;
				})
				.attr("r", 5)
				.attr("cx",0)
				.attr("cy",0)
				.attr("fill",function(d){
		      	
		      	if (d.belongsToSearchPath) {
		      		return "black";
		      	} else {
		      		return "grey";
		      	}
				
			});

		    this.node.exit().remove();

		    this.links = this.svg.selectAll(".link");
			this.link = this.links.data(this.model.links);

			this.link
				.attr("id",function(d) {
					return d.id;
				})
				.attr("stroke", function(d) {
			      	if (d.target.belongsToSearchPath) {
			      		return "black";
			      	} else {
			      		return "grey";
			      	}
			    })
		      	.attr("stroke-width", function(d) {
			      	if (d.target.belongsToSearchPath) {
			      		return "2px";
			      	} else {
			      		return "1px";
			      	}
			    });

			this.link.enter().append("line")
		  		.attr("id",function(d) {
		  		return d.id;
		  	})

		  	.attr("stroke",function(d) {
		      	if (d.target.belongsToSearchPath) {
		      		return "black";
		      	} else {
		      		return "grey";
		      	}
		  	  })
		  	  .attr("fill", "none")
		  	  .attr("stroke-width",function(d) {
		      	if (d.target.belongsToSearchPath) {
		      		return "2px";
		      	} else {
		      		return "1px";
		      	}
		   
		  	  })
		  	  .attr("class", "link");
		  
		  	  this.link.exit().remove();
		      /*
		      .on("mouseover", mouseover)
      		  .on("mouseout", mouseout)
      		  .on("mousedown",function (d) {
      		  	my.mousedown(d);
      		  });
			  */
			this.force = d3.layout.force()
				.size([this.width, this.height])
				.nodes(this.model.nodes)
				.links(this.model.links)
				.linkDistance(120)
				.charge(-120)
				.on("tick", this.tick);
			this.force.start();
		},

		tick : function() {
			this.node.attr("transform", function(d) {
  		  		var tr = "translate(" + d.x + "," + d.y + ")";
  				return tr; 
  			});

			this.link.attr("x1", function(d) {return d.source.x; })
		    	.attr("y1", function(d) { return d.source.y; })
		    	.attr("x2", function(d) { 
		    		//console.log("target", d.target);
		    		return d.target.x; })
		    	.attr("y2", function(d) { return d.target.y; });
		},



		nodeClick: function(evt) {
			console.log("currentTarget ", evt.currentTarget);
			//console.log(this);
			if (this.model) {
				//this.model.set({"relevantNodeCount":9});
				this.model.setCurrentNode($(evt.currentTarget).attr("id"));
			} else {
				console.error("model error !!!");
			}
		},
		
		
		update: function() {
			console.log("update called!");

			this.restart();
			/*
			var id = this.model.get("id"); 
			$("#" + id + " text").text(this.model.get("title"));
			var x0 = d3.select("#" + id + " circle").attr("cx");
			d3.select("#" + id + " circle")
				.transition()
				.duration(3000)
				.ease("elastic")
				.attr("cx", x0 + 20);
			d3.select("#" + id)
				.transition()
				.duration(3000)
				.ease("elastic")
				.attr("transform", "translate(450," + 200 + ")");
				*/
		},

		render: function(){


			/*
			var id = this.model.get("id"); 
			
			//$("#" + id).remove();

			var g = this.svg.append('g')
				.attr("transform","translate(450,280)")
				.attr("id",id);
			//var m = this.get("model");

			g.append("text")
				.text(this.model.get("title"))
				.attr('x', 0)
				.attr('y', -60)
				.attr("text-anchor", "middle");

			g.append("circle")
				.attr("r", 50)
				.attr("cx",0)
				.attr("cy",0)
				.attr("fill","black");
			*/
		} 
		
		
	});

	return WGraphView;
});