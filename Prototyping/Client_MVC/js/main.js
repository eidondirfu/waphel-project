// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		"underscore": {
			exports: '_'
		},
		"backbone": {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	},
	paths: {
		'jquery': 'libs/jquery/jquery',
		'underscore': 'libs/underscore/underscore',
		'backbone': 'libs/backbone/backbone',
		'd3': 'libs/d3/d3.v3'
	}
});

require([
	'jquery',
	'underscore',
	'backbone',
	'd3',
	'App'
], function($, _, Backbone, d3, App){ 
	var app = new App();
	console.log("app ", app);
}); 