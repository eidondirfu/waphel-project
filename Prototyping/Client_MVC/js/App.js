define	([
	'jquery',
	'underscore',
	'backbone',
	'models/WNode',
	'models/WGraph',
	'views/WGraphView'
], function($,_,Backbone,WNode,WGraph,WGraphView) {

	var App = function() {
		var my = {}, that = {};

		that.node = new WNode();
		that.node.create("Albert Einstein","id0");
		that.node.belongsToSearchPath = true;
		that.graph = new WGraph({"startNode" : that.node});
		that.graphview = new WGraphView({"model" : that.graph});

		return that;
	};

	return App;
});