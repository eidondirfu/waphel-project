define([], function() {
	var SimpleNode = function(title,li) {

		var my = {}, that = {};
		
		that.title = title;

		//TODO: decide how to make object conform to d3 data model
		
		that.x = Math.random()*500;
		that.y = Math.random()*300;
		that.index = 0;
		that.px = that.x;
		that.py = that.y;
		that.weight = 2;
		that.fixed = false;

		//the links array contains for the time being only
		that.targetNodes = [];

		that.belongsToSearchPath = false;

		if (li !== undefined) {
			that.addLinks(li);
		}

		that.addTargetNodes = function(li) {
			if (Object.prototype.toString.call( li ) === '[object Array]') {
				for (var it = 0; it < li.length; it++) {
					if (typeof li[it] === "string") {
						that.targetNodes.push(new SimpleNode(li[it]));
					} else {
						//TODO: make a check if li is instance of simpleNode	
						that.targetNodes.push(li[it]);
					}
				}
			} else {
				if (typeof li === "string") {
					that.targetNodes.push(new SimpleNode(li));
				} else {
					//TODO: make a check if li is instance of simpleNode
					that.targetNodes.push(li);
				}
			}
		};

		that.getTargetNodes = function() {
			return that.targetNodes;
		};

		that.getRelevantLinks = function(num) {
			var relLinks = [];
			var count = Math.min(num,that.targetNodes.length);
			for (var it = 0; it < count; it++) {
				relLinks.push({source: that, target: that.targetNodes[it]});
			}
			return relLinks;
		};

		that.getRelevantTargetNodes = function(num) {
			var relNodes = [];
			var count = Math.min(num,that.targetNodes.length);
			for (var it = 0; it < count; it++) {
				relNodes.push(that.targetNodes[it]);
			}
			return relNodes;
		}

		return that;
	};

	return SimpleNode;
});