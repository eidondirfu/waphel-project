define([

	"d3.v3"
], function() {

var singleNode = function(xCoord,yCoord,mainRadius,centerRadius,graph,svg) {

	var my = {}, that = {};

	//that.x = xCoord;
	//that.y = yCoord;
    //TODO: decide how to make object conform to d3 data model
    that.x = xCoord;
    that.y = yCoord;
    that.index = 0;
    that.px = that.x;
    that.py = that.y;
    that.weight = 2;
    that.belongsToSearchPath = false;
    //
	my.r = mainRadius;
	my.graph = graph;
	my.svg = svg;
    
    that.title = my.graph.nodes[0].title;
    that.id = -1;


	my.phi = 2*Math.PI/13.78;
    my.nodeCount = my.graph.nodes.length;

    my.normalizedDistances = [];
    my.relevancesAll = [];

    my.isoRadii = [];
    my.isoInterval = 0.01;

    my.intervalSize = my.r;
	my.visibleInterval = [0,my.intervalSize];

	// TODO define it more precisely depending on dpi of the screen
	my.totalSize = 15 * my.nodeCount; 
	my.prevZoom = 1;
	my.zoomFactor = 1/10;

	my.minRadius = centerRadius;
	my.maxRadius = my.r;


	my.scale = d3.scale.linear();
	my.scale.domain([0,my.maxRadius]);
	my.scale.range([my.minRadius, my.maxRadius]);



    my.computeIsoRadii = function() {
		for(var j = 0; j < my.nodeCount; j++) {

			my.normalizedDistances[j] = j * 1.0/my.nodeCount;
			my.relevancesAll[j] = my.graph.nodes[j].relevance;
		}

		var ceilNumber = Math.ceil(my.relevancesAll[0]/my.isoInterval);
		//var ceilIndex = 0;

		for(var i = 1; i < my.nodeCount; i++) {
			var nextCeil = Math.ceil(my.relevancesAll[i]/my.isoInterval);
			var difference = ceilNumber - nextCeil;

			/**
			*TODO weights for radius instead of arithmetic mean
			**/
			while(difference > 0) {
			var radius = (my.normalizedDistances[i] + my.normalizedDistances[i-1])/2;
				my.isoRadii.push(radius);
				difference--;
			}

			ceilNumber = nextCeil;

		}
   };

    my.drawIsoCircle = function(radius,fillColor) {
        d3.select("#isoCircle")
            .append("circle")
            .attr("cx",that.x)
            .attr("cy",that.y)
            .attr("r",radius)
            .attr("fill",fillColor)
            .attr("stroke", "white")
            .attr("stroke-width", 1);  
    };

	my.redraw = function() {

        $("#isoCircle *").remove();

        var bgCircleDrawn = false;
        var noneDrawn = true;
        var lastIndex = my.isoRadii.length - 1;

        for(var iter = my.isoRadii.length - 1; iter > 0; iter--) {
            var radius = my.isoRadii[iter]*my.totalSize;

            if(radius >= my.visibleInterval[1]) {
                lastIndex = iter;
            }

            if(radius <= my.visibleInterval[1] && radius >= my.visibleInterval[0]) {

                noneDrawn = false;
                

                if (!bgCircleDrawn) {
                    var ratio;
                    if (iter + 1 == my.isoRadii.length) {
                        ratio = 1;
                    } else {
                        ratio = my.isoRadii[iter + 1];   
                    }
                    var color = d3.rgb(255*(1-ratio),0,255*ratio);
                    my.drawIsoCircle(my.maxRadius, color);
                    bgCircleDrawn = true;
                }

                var rr = my.scale(radius - my.visibleInterval[0]);
                var color = d3.rgb(255*(1-radius/my.totalSize),0,255*(radius/my.totalSize));
                my.drawIsoCircle(rr,color);
            }
        }

        if (noneDrawn) {
            console.log("draw none!" + lastIndex);
            var ratio = my.isoRadii[lastIndex];
            var color = d3.rgb(255*(1-ratio),0,255*ratio);
            my.drawIsoCircle(my.maxRadius,color);
        }

		for(var it=1; it < my.graph.nodes.length; it++){
            
			var nodeRadius = my.normalizedDistances[it]*my.totalSize;
            var lambda = my.phi * it;

            if(nodeRadius <= my.visibleInterval[1] && nodeRadius >= my.visibleInterval[0]) {

                var r = nodeRadius - my.visibleInterval[0];
                var x = that.x + my.scale(r) * Math.cos(lambda);
                var y = that.y + my.scale(r) * Math.sin(lambda);
                //console.log(lambda + " " + x + " " + y);

                
                $("#" + it)
                    .attr("transform","translate(" + x + "," + y + ")")
                    .attr("opacity",1.0);
                $("#" + it + " text")
                    .attr("fill",d3.rgb(255,255,255));
                $("#" + it + " circle")
                    .attr("r", 5);
            
            } else {
                $("#" + it)
                    .attr("opacity",0.0);
            }
            /*else if(nodeRadius > my.visibleInterval[1]) {
                var ratio = (my.totalSize-nodeRadius)/(my.totalSize-my.visibleInterval[1]);
                var x0 = that.x + (my.maxRadius + 5 + 20 * (1 - ratio)) * Math.cos(lambda);
                var y0 = that.y + (my.maxRadius + 5 + 20 * (1 - ratio)) * Math.sin(lambda);
                
                $("#" + it)
                    .attr("transform","translate(" + x0 + "," + y0 + ")")
                    //.attr("opacity",ratio);
                $("#" + it + " text")
                    .attr("fill","none");
                $("#" + it + " circle")
                    .attr("r", ratio * 4 + 1);
            } else if ( nodeRadius < my.visibleInterval[0]){
                var ratio = nodeRadius/my.visibleInterval[0];
                var x0 = that.x + (my.minRadius - 5) * Math.cos(lambda);
                var y0 = that.y + (my.minRadius - 5) * Math.sin(lambda);

                $("#" + it)
                    .attr("transform","translate(" + x0 + "," + y0 + ")")
                    .attr("opacity",ratio);
                $("#" + it + " text")
                    .attr("fill","none");
                $("#" + it + " circle")
                    .attr("r", ratio*5);
            }*/

            
        }
	};




    my.zoom = function(evt) {

        console.log(evt);

        if (evt.sourceEvent.type === "mousewheel") {
    		//var nextZoom = evt.scale;
    		//var zoom = nextZoom - prevZoom;
    		//prevZoom = nextZoom;
    		var zoom = evt.sourceEvent.wheelDeltaY * my.zoomFactor;

    		if(zoom + my.visibleInterval[0] < 0) {
    			zoom = -my.visibleInterval[0];
    		} else if(zoom + my.visibleInterval[1] > my.totalSize) {
    			zoom = my.totalSize - my.visibleInterval[1];
    		}
    		my.visibleInterval[0] += zoom;
    		my.visibleInterval[1] += zoom;
    		my.redraw();
        }
	};
        
    that.create = function() {
        my.computeIsoRadii();

		var group = my.svg
            //.append('svg:g')
            .call(d3.behavior.zoom().on("zoom", function(){
                //console.log(d3.event);
                my.zoom(d3.event);
            }))
            .append('svg:g');

        

        var circ = group.append('svg:circle')
            .attr('cx', that.x)
            .attr('cy', that.y)
            .attr('r',my.r)
            .attr("stroke", "grey")
            .attr("stroke-width", 5)
            .attr('fill', 'white');

        var isoCircles = group.append('g').attr("id","isoCircle");

        circ.call(d3.behavior.zoom().on("zoom", my.draw(group)));

        //my.draw(group);

        group.call(my.redraw);


    };

    that.createSimple = function() {
        
        /*
        var simpleNode = my.svg.append("g");

        simpleNode.append("circle")
            .attr("cx",that.x)
            .attr("cy",that.y)
            .attr("r", 5)
            .attr("fill","black");
        
        simpleNode.append("text")
            .text(my.graph.nodes[0].title)
            .attr('x', that.x)
            .attr('y', that.y - 10)
            .attr("fill","black")
            .attr("text-anchor", "middle");    
        */
    };
    
    that.getTitle = function() {
            
        return that.title;
    };


    that.getRelevantLinks = function(num) {

        var linkList = [];

        for(var i = 1; i <= num; i++) {
            /// TODO: make this work!
            var n = new singleNode(that.x,that.y,my.maxRadius,my.minRadius,graph,svg);
            n.createSimple();
            n.title = my.graph.nodes[Math.floor(Math.random()*my.graph.nodes.length)].title;
            //n.title = "other";
            var link = {source: that, target: n};
            linkList.push(link);
        }
        //console.log("linklist ", linkList);
        return linkList;

    };

    that.getRandomTitle = function () {
        return my.graph.nodes[Math.floor(Math.random()*my.graph.nodes.length)].title;
    }
    
    that.getRandomTargetNode = function() {
            var n = new singleNode(that.x,that.y,my.maxRadius,my.minRadius,graph,svg);
            n.createSimple();
            n.title = that.getRandomTitle();        
            return n;
    }
        /*
        function redraw() {
          console.log("here", d3.event.translate, d3.event);
          svg.attr("transform",
              "translate(" + d3.event.translate + ")"
              + " scale(" + d3.event.scale + ")");
        }*/

    my.draw = function(group) {
        
        /*
        var link = group.selectAll(".link")
                .data(my.graph.links)
                .enter().append("line")
                .attr("class", "link");
        */

        var node = group.selectAll(".node")
                .data(my.graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("id",function(d,i){return i;})
                .attr("transform",function(d,i){

                    var x = that.x;
                    var y = that.y;
                    if(i !== 0) {
                        x = that.x + 5 * i * Math.cos(i*my.phi);
                        y = that.y + 5 * i * Math.sin(i*my.phi);
                        //x = width/2 + 1000*(1 - d.relevance) * Math.cos(i*phi);
                        //y = height/2 + 1000*(1 - d.relevance) * Math.sin(i*phi);
                    } 

                    return "translate(" + x + "," + y + ")";
                });
                //.call(force.drag);

        node.append("text")
            .text(function(d) { return d.title; })
            .attr('x', 0)
            .attr('y', -10)
            .attr("bg-color","white")
            .attr("text-anchor", "middle");
        
        node.append("circle")
            .attr("cx",1)
            .attr("cy",1)
            .attr("r", 5)
            .attr("fill","white");

    };

   
	return that;
};

	return singleNode;
});