RelevanceRadar rr;

void setup() {
  size(800, 800, JAVA2D);
  rr = new RelevanceRadar(width/2, height/2, 750, 750);
  smooth();
}

void draw() {
  background(0);
  rr.display(this);
}

void mouseDragged() {
  rr.drag(mouseX, mouseY, pmouseX, pmouseY);
}

void keyPressed() {
  switch(key) {
  case 'q':
    rr.drag(1, 1, 0, 0);
    break;
  case 'a':
    rr.drag(1, 1, 0, 0);
    break;
  }
}

ArrayList<Relevance> generateRelevances(int count, int countPerSegment) {
  ArrayList<Relevance> result = new ArrayList<Relevance>();
  float phiInc = TWO_PI/float(countPerSegment);
  float count_1 = 1/float(count);
  float phi = 0f;
  for (int i = 0; i < count; i++) {
    float r = random(count_1*i,count_1*(i+1));
    Relevance rel = new Relevance(r,phi);
    result.add(rel);
    phi = (phi+phiInc)%TWO_PI;
    //phi = (phi+random(phiInc*2))%TWO_PI;
  }
  return result;
}  

