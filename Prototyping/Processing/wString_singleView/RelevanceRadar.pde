class RelevanceRadar {
  // position, visible and actual (relevant-) dimensions
  float rfactor = 5;
  float x0, y0, w, rw, h, rh;
  // relevance interval endpoints
  float r0, r1;
  color c0 = #0000FF, c1 = #FF0000;
  //interval of the "radar lines"
  float rInt = 0.02f;
  float rStep;
  float nodeSize = 100f;
  float rMin,rMax;
  ArrayList<Relevance> rels = generateRelevances(200,16);
  
  RelevanceRadar(float x, float y, float w, float h) {
    this.x0 = x;
    this.y0 = y;
    this.w = w;
    this.h = h;
    rw = w*rfactor;
    rh = h*rfactor;
    r0 = 1f;
    r1 = r0 - 1f/rfactor;
    rStep = rw*rInt;
    rMax = w;
    rMin = nodeSize;
  }
  
  void display(PApplet p5) {
    p5.ellipseMode(CENTER);
    p5.pushStyle();
    p5.stroke(255);

    float rad1 = ceil(r1/rInt)*rInt - r1;
    float r = r1 + rad1;
    float inc = norm(r-rInt,0,1f);
      color c = p5.lerpColor(c0,c1,inc);
      p5.fill(c);
      p5.ellipse(x0,y0,rMax,rMax);

    while (r <= r0) {
      float screenR = p5.map(r,r1,r0,rMax,rMin);
      inc = p5.norm(r,0f,1f);
      c = p5.lerpColor(c0,c1,inc);
      p5.fill(c);
      p5.ellipse(x0,y0,screenR,screenR);
      r+= rInt;
    }
    p5.noStroke();
    p5.fill(255,127);
    p5.ellipse(x0,y0,nodeSize,nodeSize);
    p5.popStyle();
    for (Relevance rel : rels) {
      rel.display(p5,this);
    }
  }
  
  boolean mouseOver(int x, int y) {
    if (dist(x,y,x0,y0) <= w*0.5f) {
      return true;
    }
    return false;
  }
  
  void drag(int mx, int my, int pmx, int pmy) {
    if(mouseOver(pmx,pmy)) {
      PVector d = new PVector(mx-pmx, my-pmy);
      PVector m0 = new PVector(pmx-x0, pmy-y0);
      m0.normalize();
      float drag = PVector.mult(d,m0).mag()*rInt*0.01f;
      if (PVector.angleBetween(m0,d) >= HALF_PI) drag*=-1;
      //drag = dist(mx,my,pmx,pmy);
      if (r0 + drag >= 1f) {
        drag = 1-r0;
      } else if (r1 + drag <= 0f) {
        drag = -r1;
      }
      r0 += drag;
      r1 += drag;      
    }
  }
}
