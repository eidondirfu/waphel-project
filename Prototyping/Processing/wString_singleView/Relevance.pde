class Relevance {
  float value, phi;
  Relevance(float value, float phi) {
    this.value = value;
    this.phi = phi;
  }
  
  void display(PApplet p5, RelevanceRadar rr) {
    if (value >= rr.r1 && value <= rr.r0) {
      p5.ellipseMode(CENTER);
      p5.pushStyle();
      p5.fill(255,127);
      p5.stroke(255,127);
      float rad = 10;
      float r = p5.map(value,rr.r1,rr.r0,rr.rMax*0.5f,rr.rMin*0.5f);
      float x = rr.x0 + cos(phi)*r;
      float y = rr.y0 + sin(phi)*r;
      p5.ellipse(x,y,rad*2,rad*2);
      p5.line(rr.x0 + rr.nodeSize*0.5f*cos(phi),rr.y0 + rr.nodeSize*0.5f*sin(phi),x-cos(phi)*rad,y-sin(phi)*rad);
      p5.popStyle();
    }
  }
}
