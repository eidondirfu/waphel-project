package Neo4jGraphDatabase;


public class Page {

	private String pageTitle;
	private String pageRedirectTitle;



	public Page() {

	}

	public String toString() {

				return " title: " + this.pageTitle;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPageRedirectTitle() {
		return pageRedirectTitle;
	}

	public void setPageRedirectTitle(String pageRedirectTitle) {
		this.pageRedirectTitle = pageRedirectTitle;
	}

	
	

}
