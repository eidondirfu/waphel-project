package Neo4jGraphDatabase;

import java.util.Map;

import org.neo4j.helpers.collection.MapUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class WikipediaVertexCreator implements ContentHandler {

	private String currentValue;
	private long pageNumber = 0;
	private int redirectNumber = 0;
	private Page page;
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {

		if (qName.equals("page")) {
			
			page = new Page();

		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		if (qName.equals("title")) {

			page.setPageTitle(currentValue);

		}


		if (qName.equals("redirect")) {
			page.setPageRedirectTitle(currentValue);
		}
		
		if (qName.equals("page")) {

			if (page.getPageRedirectTitle() == null) {
				pageNumber++;
				Map<String, Object> properties = MapUtil.map("title", page.getPageTitle(),"backNumber",0,"forthNumber",0);
				long nodeId = WikipediaGraphCreator.batchInserter
						.createNode(properties);
				// WikipediaGraphCreator.batchInserter.createNode(pageNumber,
				// properties);
				WikipediaGraphCreator.idByTitleMap.put(page.getPageTitle().toLowerCase(), nodeId);
				WikipediaGraphCreator.batchIndex.add(nodeId, properties);
				if (pageNumber % 50000 == 0) {
					WikipediaGraphCreator.batchIndex.flush();
					
					System.out.println(pageNumber + " vertex created: "
							+ page.getPageTitle());
				}
				
			} else {
				WikipediaGraphCreator.redirectTitleMap.put(page.getPageTitle().toLowerCase(),
						page.getPageRedirectTitle().toLowerCase());
				
				redirectNumber++;
				
				if(redirectNumber % 50000 == 0) {
					System.out.println(redirectNumber + " REDIRECT: "
							+ page.getPageTitle());
				}
				
				
			}

		}
	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startPrefixMapping(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

}
