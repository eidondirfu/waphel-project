package Neo4jGraphDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.index.lucene.unsafe.batchinsert.LuceneBatchInserterIndexProvider;
import org.neo4j.unsafe.batchinsert.BatchInserter;
import org.neo4j.unsafe.batchinsert.BatchInserterIndex;
import org.neo4j.unsafe.batchinsert.BatchInserterIndexProvider;
import org.neo4j.unsafe.batchinsert.BatchInserters;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


public class WikipediaGraphCreator {

	protected static BatchInserter batchInserter;
	protected static BatchInserterIndex batchIndex;
	protected static Map<String, Long> idByTitleMap;
	protected static Map<String,String> redirectTitleMap;
	protected static final String GRAPH_DB_PATH = "/Users/eidonfiloi/Downloads/neo4j-community-1.9.M03/data/waphelGraphSingleEdgeForth.db";
	protected static final String XML_INPUT_PATH = "/Users/eidonfiloi/Documents/WAPHEL/waphel-20130102-pages-and-links-fine.xml";

	protected static int numberOfLinks = 0;

	protected static InputStreamReader reader;
	protected static InputStreamReader reader2;
	protected static final WikipediaVertexCreator VERTEX_HANDLER = new WikipediaVertexCreator();
	protected static final WikipediaEdgeCreator EDGE_HANDLER = new WikipediaEdgeCreator();

	public static void main(String[] args) throws Exception {

		Map<String, String> config = new HashMap<String, String>();
		config.put("use_memory_mapped_buffers", "true");
		config.put("neostore.propertystore.db.index.keys.mapped_memory", "10G");
		config.put("neostore.propertystore.db.index.mapped_memory", "10G");
		config.put("neostore.nodestore.db.mapped_memory", "90M");
		config.put("neostore.relationshipstore.db.mapped_memory", "5G");
		config.put("neostore.propertystore.db.mapped_memory", "100M");
		config.put("neostore.propertystore.db.strings.mapped_memory", "150M");
		config.put("neostore.propertystore.db.arrays.mapped_memory", "100M");
		config.put("cache_type", "weak");

		batchInserter = BatchInserters.inserter(GRAPH_DB_PATH, config);

		final BatchInserterIndexProvider indexProvider = new LuceneBatchInserterIndexProvider(
				batchInserter);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				indexProvider.shutdown();
				batchInserter.shutdown();
			}
		});

		idByTitleMap = new HashMap<String,Long>();
		redirectTitleMap = new HashMap<String,String>();

		batchIndex = indexProvider.nodeIndex("pages",
				MapUtil.stringMap("type", "exact", "to_lower_case", "false"));
		batchIndex.setCacheCapacity("title", 10000000);

		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();

			File file = new File(XML_INPUT_PATH);
			InputStream inputStream = new FileInputStream(file);
			reader = new InputStreamReader(inputStream, "UTF-8");
			InputSource inputSource = new InputSource(reader);
			inputSource.setEncoding("UTF-8");

			xmlReader.setContentHandler(VERTEX_HANDLER);

			long start = System.currentTimeMillis();
			System.out.println("parse started...");
			xmlReader.parse(inputSource);
			long end = System.currentTimeMillis();
			System.out.println("total time: " + (end - start));

			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();

			File file = new File(XML_INPUT_PATH);
			InputStream inputStream = new FileInputStream(file);
			reader = new InputStreamReader(inputStream, "UTF-8");
			InputSource inputSource = new InputSource(reader);
			inputSource.setEncoding("UTF-8");

			xmlReader.setContentHandler(EDGE_HANDLER);

			long start = System.currentTimeMillis();
			System.out.println("parse started...");
			xmlReader.parse(inputSource);
			indexProvider.shutdown();
			batchInserter.shutdown();
			long end = System.currentTimeMillis();
			System.out.println("total time: " + (end - start));

			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}

	}

}
