package Neo4jGraphDatabase;

import java.util.Map;

import org.neo4j.helpers.collection.MapUtil;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class WikipediaEdgeCreator implements ContentHandler {

	private EdgePage page;
	private long linkNumber = 0;
	private String currentValue;

	private int badLinkNumber = 0;

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		currentValue = new String(ch, start, length);

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {

		if (qName.equals("page")) {
			page = new EdgePage();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (qName.equals("title")) {
			// vertexId =
			// WikipediaGraphCreator.batchIndex.get("title",currentValue).getSingle();
			page.setVertexId(WikipediaGraphCreator.idByTitleMap
					.get(currentValue.toLowerCase()));

			// System.out.println("start vertex: " + vertexId);

		}

		if (qName.equals("redirect")) {

			page.setRedirect(true);
		}

		if (qName.equals("link")) {
			// Map<String, Object> properties = MapUtil.map("relevance", 1);
			// Long linkVertexId =
			// WikipediaGraphCreator.batchIndex.get("title",
			// currentValue).getSingle();

			if (!page.isRedirect()) {
				//try {
					Long linkId = WikipediaGraphCreator.idByTitleMap
							.get(currentValue.toLowerCase());
					if (linkId != null) {

						page.getLinkIDs().add(linkId);
						// System.out.println("link created: " + linkVertexId);
					} 
					
					else {
						/*
						Long capVertexId = WikipediaGraphCreator.idByTitleMap
								.get(WordUtils.capitalize(currentValue));
						if (capVertexId != null) {

							page.getLinkIDs().add(capVertexId);
						} 
						*/
						String redirectTitle = WikipediaGraphCreator.redirectTitleMap
								.get(currentValue.toLowerCase());
						if (redirectTitle != null) {
							Long redirectVertexId = WikipediaGraphCreator.idByTitleMap
									.get(redirectTitle);
							if(redirectVertexId != null) {
								page.getLinkIDs().add(redirectVertexId);
							}
						} else {
							badLinkNumber++;
							if (badLinkNumber % 50000 == 0) {
								System.out.println("BAD LINK "
										+ badLinkNumber + " "
										+ currentValue);

							}
						}
						
					}

				//} catch (NullPointerException e) {

				//}

			}

		}

		if (qName.equals("page")) {

			//try {
			if (!page.getLinkIDs().isEmpty()  && (page.getVertexId() != null)) {

				for (Long id : page.getLinkIDs()) {
					WikipediaGraphCreator.batchInserter.createRelationship(
							page.getVertexId(), id, WikipediaRelationType.LINK,
							null);
					linkNumber++;
				}
				
				WikipediaGraphCreator.batchInserter.setNodeProperty(page.getVertexId(), "forthNumber",page.getLinkIDs().size());
				
				if (linkNumber % 50000 == 0) {
					// WikipediaGraphCreator.batchIndex.flush();
					System.out.println(linkNumber + " link created "
							+ currentValue);

				}
				

			}
			
			//} catch(NullPointerException e) {
				//System.out.println("null pointer " + currentValue);
				//e.printStackTrace();
			//}
			
			
		}

	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String target, String data)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator locator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String name) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {
		// TODO Auto-generated method stub

	}

}
