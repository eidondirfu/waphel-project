package Neo4jGraphDatabase;

import java.util.HashSet;


public class EdgePage {
	
	private boolean isRedirect;
	private Long vertexId;
	private HashSet<Long> linkIDs;
	private int size;

	
	public EdgePage() {
		
		this.isRedirect = false;
		this.linkIDs = new HashSet<Long>();
		this.vertexId = null;
		this.size = 0;
	}

	public boolean isRedirect() {
		return this.isRedirect;
	}

	public void setRedirect(boolean isRedirect) {
		this.isRedirect = isRedirect;
	}

	
	public HashSet<Long> getLinkIDs() {
		
		if(this.linkIDs == null) {
			this.linkIDs = new HashSet<Long>();
		}
		return this.linkIDs;
	}

	public void setLinkIDs(HashSet<Long> linkIDs) {
		this.linkIDs = linkIDs;
	}

	public Long getVertexId() {
		return this.vertexId;
	}

	public void setVertexId(Long vertexId) {
		this.vertexId = vertexId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = linkIDs.size();
	}
	
	
	
	
	
	
	
	
	
	

}
