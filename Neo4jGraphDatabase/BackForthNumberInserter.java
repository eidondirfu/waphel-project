package Neo4jGraphDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.graphdb.index.ReadableIndex;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.kernel.EmbeddedReadOnlyGraphDatabase;
import org.neo4j.tooling.GlobalGraphOperations;

import com.google.common.collect.Lists;

public class BackForthNumberInserter {

	protected static final String DB_PATH = "/Users/eidonfiloi/Downloads/neo4j-community-1.9.M03/data/waphelGraphSingleEdgeForth.db";
	protected static GraphDatabaseService graphDB;
	protected static IndexManager indexManager;
	protected static ReadableIndex<Node> index;
	protected static ArrayList<Node> allNodes;
	protected static ArrayBlockingQueue<Object> allTheNodes;
	protected static final Object POISON_PILL = new Object();

	// protected static Node startNode;

	protected static ArrayList<Node> adjacentNodes;

	protected static int numberOfNodes = 0;

	protected static Object lock = new Object();

	protected static int SIZE;

	public static void main(String[] args) {

		Map<String, String> config = new HashMap<String, String>();
		config.put("use_memory_mapped_buffers", "true");
		config.put("neostore.nodestore.db.mapped_memory", "90M");
		config.put("neostore.relationshipstore.db.mapped_memory", "5G");
		config.put("neostore.propertystore.db.mapped_memory", "1G");
		config.put("neostore.propertystore.db.strings.mapped_memory", "500M");
		config.put("cache_type", "weak");

		graphDB = new EmbeddedGraphDatabase(DB_PATH, config);
		registerShutdownHook(graphDB);

		indexManager = graphDB.index();
		index = indexManager.forNodes("pages");

		GlobalGraphOperations global = GlobalGraphOperations.at(graphDB);

		allNodes = Lists.newArrayList(global.getAllNodes());

		SIZE = allNodes.size();

		allTheNodes = new ArrayBlockingQueue<Object>(10000000);

		for (Node node : allNodes) {
			allTheNodes.add(node);
		}
		allTheNodes.add(POISON_PILL);

		long start = System.currentTimeMillis();
		System.out.println("starting...");

		ExecutorService executor = Executors.newCachedThreadPool();

		for (int i = 0; i < 100; i++) {

			executor.submit(new Runnable() {

				@Override
				public void run() {
					try {
						worker();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			});

		}

		executor.shutdown();

		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		long end = System.currentTimeMillis();

		System.out.println("time: " + (end - start));

		graphDB.shutdown();

	}

	private static void worker() throws InterruptedException {

		Transaction tx = graphDB.beginTx();

		int i = 0;
		try {
			while (true) {
				Object node = allTheNodes.take();
				if (node.equals(POISON_PILL)) {
					allTheNodes.add(POISON_PILL);
					break;
				}

				ArrayList<Relationship> backEdges = Lists
						.newArrayList(((Node) node)
								.getRelationships(Direction.INCOMING));
				// ArrayList<Relationship> forthEdges =
				// Lists.newArrayList(((Node)node).getRelationships(Direction.OUTGOING));

				synchronized (lock) {
					numberOfNodes++;
				}

				i++;

				((Node) node).setProperty("backNumber", backEdges.size());
				System.out.println(numberOfNodes + " "
						+ ((Node) node).getProperty("title"));
				if (i % 5000 == 0) {
					tx.success();
					tx.finish();
					tx = graphDB.beginTx();
					System.out.println(Thread.currentThread().getName()
							+ ": flush");
				}

			}
			tx.success();
		} finally {
			tx.finish();
		}
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDB) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running example before it's completed)
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDB.shutdown();
			}
		});
	}

}