import javax.xml.stream.XMLStreamException;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class WikipediaContentHandler implements ContentHandler {

	private String currentValue;
	private WikipediaPage page;

	private StringBuilder wikipediaText;
	private String currentTag;

	private WikipediaLinkParser linkParser = null;

	private long pageNumber = 0;

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (currentTag.equals("text")) {
			wikipediaText = wikipediaText.append(ch, start, length);
		}

		currentValue = new String(ch, start, length);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {

		currentTag = qName;

		if (qName.equals("page")) {
			page = new WikipediaPage();
			wikipediaText = new StringBuilder("");
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (localName.equals("title")) {
			if ((currentValue.contains(":") || currentValue
					.contains("(disambiguation)")) == false) {
				page.setPageTitle(currentValue);

			}
		}

		if (localName.equals("text")) {
			page.setPageText(wikipediaText.toString());
		}

		if (qName.equals("page")) {

			if (page.getPageTitle() != null) {

				linkParser = new WikipediaLinkParser(wikipediaText.toString());
				page.setPageLinks(linkParser.getPageLinks());
				try {
					WikipediaParser.writer.writeStartElement("page");
					WikipediaParser.writer.writeStartElement("title");
					WikipediaParser.writer.writeCharacters(page.getPageTitle());
					WikipediaParser.writer.writeEndElement();

					for (String link : page.getPageLinks()) {
						WikipediaParser.writer.writeStartElement("link");
						WikipediaParser.writer.writeCharacters(link);
						WikipediaParser.writer.writeEndElement();
					}
					WikipediaParser.writer.writeEndElement();

					pageNumber++;

					System.out.println(pageNumber + " page processed "
							+ page.getPageTitle());

				} catch (XMLStreamException e) {
					e.printStackTrace();
				}
			} else {
				// System.out.println(" page processed " + page.getPageTitle());

			}

		}
	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startPrefixMapping(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

}
