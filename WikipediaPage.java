import java.util.Vector;

public class WikipediaPage {

	private String pageTitle;
	private String pageText;
	private Vector<String> pageLinks;

	public WikipediaPage() {

		pageLinks = new Vector<String>();
	}

	public String toString() {

		StringBuilder links = new StringBuilder();

		for (String link : pageLinks) {
			links.append("' " + link + " '");
		}
		return " title: " + this.pageTitle + " links: " + links.toString();
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Vector<String> getPageLinks() {
		return pageLinks;
	}

	public void setPageLinks(Vector<String> pageLinks) {
		this.pageLinks = pageLinks;
	}

	public String getPageText() {
		return pageText;
	}

	public void setPageText(String pageText) {
		this.pageText = pageText;
	}

}
