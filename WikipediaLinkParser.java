import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikipediaLinkParser {

	private String wikipediaText = null;
	private Vector<String> pageLinks = null;

	public WikipediaLinkParser(String wikipediaText) {
		this.wikipediaText = wikipediaText;
	}

	public Vector<String> getPageLinks() {

		if (pageLinks == null) {
			parseLinks();
		}
		return pageLinks;
	}

	private void parseLinks() {

		pageLinks = new Vector<String>();

		Pattern pat = Pattern.compile("\\[\\[(.*?)\\]\\]", Pattern.MULTILINE);
		Matcher mat = pat.matcher(wikipediaText);

		while (mat.find()) {

			String[] temp = mat.group(1).split("\\|");
			if (temp == null || temp.length == 0) {
				continue;
			}

			String link = temp[0];
			if (link.contains(":") == false) {
				if (link.contains("#")) {
					int index = link.indexOf("#");
					if (index > 0) {
						String trimLink = link.substring(0, index);
						pageLinks.add(trimLink.replace('_', ' '));
					}

				}
				else {
					pageLinks.add(link.replace('_', ' '));

				}
			} 
		}
	}

}
